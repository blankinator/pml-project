graph [
  node [
    id 0
    label "Chursdorf, Penig"
  ]
  node [
    id 1
    label "Gautzscher Platz, Markkleeberg"
  ]
  node [
    id 2
    label "Pfingstweide, Leipzig"
  ]
  node [
    id 3
    label "Kleiststr., Leipzig"
  ]
  node [
    id 4
    label "Am Ehrenfriedhof, Eilenburg"
  ]
  node [
    id 5
    label "Suedstr., Groitzsch b Pegau"
  ]
  node [
    id 6
    label "Wellaune Siedlung, Bad Dueben"
  ]
  node [
    id 7
    label "Gohlis-Sued, Leipzig"
  ]
  node [
    id 8
    label "Dewitz Am Winneberg, Taucha (Sachsen)"
  ]
  node [
    id 9
    label "Am Vorwerk, Leipzig"
  ]
  node [
    id 10
    label "Hermann-Liebmann-Str./Eisenbahnstr., Leipzig"
  ]
  node [
    id 11
    label "Hohenheida Lindengasse, Leipzig"
  ]
  node [
    id 12
    label "Morgensternstr., Leipzig"
  ]
  node [
    id 13
    label "S-Bahnhof Moeckern, Leipzig"
  ]
  node [
    id 14
    label "Thronitz, Markranstaedt"
  ]
  node [
    id 15
    label "Miltitz Geschwister-Scholl-Str., Leipzig"
  ]
  node [
    id 16
    label "Nossen Markt"
  ]
  node [
    id 17
    label "Zschortauer Str., Leipzig"
  ]
  node [
    id 18
    label "Taubestr., Leipzig"
  ]
  node [
    id 19
    label "Leipziger Str., Eilenburg"
  ]
  node [
    id 20
    label "Schkeuditzer Str., Markranstaedt"
  ]
  node [
    id 21
    label "Hohenheida Am Anger, Leipzig"
  ]
  node [
    id 22
    label "Friedrich-Ebert-Str./Schule, Wurzen"
  ]
  node [
    id 23
    label "Gewerbegebiet Podelwitz, Leipzig"
  ]
  node [
    id 24
    label "Wachau Am Bach, Markkleeberg"
  ]
  node [
    id 25
    label "Congress-Center, Leipzig"
  ]
  node [
    id 26
    label "Breitenhain, Lucka"
  ]
  node [
    id 27
    label "Liebertwolkwitz Zur alten Sandgrube, Leipzig"
  ]
  node [
    id 28
    label "Nonnenstr., Leipzig"
  ]
  node [
    id 29
    label "Pestalozzistr., Zwenkau"
  ]
  node [
    id 30
    label "Wachau Crostewitzer Str., Markkleeberg"
  ]
  node [
    id 31
    label "Quering Landstr., Neukyhna"
  ]
  node [
    id 32
    label "Boehlitz-Ehrenberg Ludwig-Jahn-Str., Leipzig"
  ]
  node [
    id 33
    label "Moelkau Schulstr., Leipzig"
  ]
  node [
    id 34
    label "Meusdorf, Leipzig"
  ]
  node [
    id 35
    label "Kulkwitz Kirche, Markranstaedt"
  ]
  node [
    id 36
    label "Schoenefeld-Ost Verbundnetz Gas AG, Leipzig"
  ]
  node [
    id 37
    label "Bushof, Zwenkau"
  ]
  node [
    id 38
    label "Otto-Michael-Str., Leipzig"
  ]
  node [
    id 39
    label "Liebertwolkwitzer Markt, Leipzig"
  ]
  node [
    id 40
    label "Ferag, Leipzig"
  ]
  node [
    id 41
    label "Gollmenz Abzw. n. Woelkau, Schoenwoelkau"
  ]
  node [
    id 42
    label "Kleinpoesna, Leipzig"
  ]
  node [
    id 43
    label "Schwarzer Berg, Taucha (Sachsen)"
  ]
  node [
    id 44
    label "Parkstr. agra, Markkleeberg"
  ]
  node [
    id 45
    label "Adler, Leipzig"
  ]
  node [
    id 46
    label "An der Schaeferei, Taucha (Sachsen)"
  ]
  node [
    id 47
    label "Saarlaender Str., Leipzig"
  ]
  node [
    id 48
    label "Hugo-Aurig-Str., Leipzig"
  ]
  node [
    id 49
    label "Rossplatz, Leipzig"
  ]
  node [
    id 50
    label "Lindenhayn Abzw. n. Noitzsch, Zschepplin"
  ]
  node [
    id 51
    label "Feuerbachstr., Leipzig"
  ]
  node [
    id 52
    label "Damaschkestr., Grosspoesna"
  ]
  node [
    id 53
    label "Mockau-West, Leipzig"
  ]
  node [
    id 54
    label "Taucha (Sachsen) Rathaus"
  ]
  node [
    id 55
    label "Koburger Bruecke, Leipzig"
  ]
  node [
    id 56
    label "Friederikenstr., Leipzig"
  ]
  node [
    id 57
    label "Dachauer Str., Leipzig"
  ]
  node [
    id 58
    label "Engelsdorf Gymnasium, Leipzig"
  ]
  node [
    id 59
    label "Bad Dueben B2 Profiroll"
  ]
  node [
    id 60
    label "Neue Gaststaette, Grosspoesna"
  ]
  node [
    id 61
    label "Lindenthal Rudolf-Breitscheid-Str., Leipzig"
  ]
  node [
    id 62
    label "Glesien Karl-Liebknecht-Str., Schkeuditz"
  ]
  node [
    id 63
    label "Sachsenpark, Leipzig"
  ]
  node [
    id 64
    label "Seegeritz Mitte, Taucha (Sachsen)"
  ]
  node [
    id 65
    label "Eutritzscher Zentrum, Leipzig"
  ]
  node [
    id 66
    label "Heinrich-Heine-Str., Borsdorf"
  ]
  node [
    id 67
    label "Am Teich, Zwochau"
  ]
  node [
    id 68
    label "Auenhain, Markkleeberg"
  ]
  node [
    id 69
    label "Zitzschen Grossdalziger Str., Zwenkau"
  ]
  node [
    id 70
    label "Allee-Center Offenburger Str., Leipzig"
  ]
  node [
    id 71
    label "Grossdeuben Martin-Luther-Str., Boehlen b Leipzig"
  ]
  node [
    id 72
    label "Gaschwitz Bahnhof, Markkleeberg"
  ]
  node [
    id 73
    label "Panitzscher Str., Borsdorf"
  ]
  node [
    id 74
    label "Knauthain Am Weiher, Leipzig"
  ]
  node [
    id 75
    label "Fussgaengertunnel, Espenhain"
  ]
  node [
    id 76
    label "Fockestr., Leipzig"
  ]
  node [
    id 77
    label "Freiberg (Sachsen) Busbahnhof"
  ]
  node [
    id 78
    label "Gertitz, Delitzsch"
  ]
  node [
    id 79
    label "Roehrsdorf Wasserschaenkenstr/B95, Chemnitz"
  ]
  node [
    id 80
    label "Weinbrennerstr., Leipzig"
  ]
  node [
    id 81
    label "Einertstr., Leipzig"
  ]
  node [
    id 82
    label "Cunnersdorfer Str., Leipzig"
  ]
  node [
    id 83
    label "Taucha (Sachsen) Markt"
  ]
  node [
    id 84
    label "Dreiskau-Muckern Muckern, Grosspoesna"
  ]
  node [
    id 85
    label "Paunsdorf Strassenbahnhof, Leipzig"
  ]
  node [
    id 86
    label "Gotthardstr., Merseburg"
  ]
  node [
    id 87
    label "Niederfrohnaer Weg, Muehlau"
  ]
  node [
    id 88
    label "Seumestr., Leipzig"
  ]
  node [
    id 89
    label "Schwimmhalle Suedost, Leipzig"
  ]
  node [
    id 90
    label "Zechauer Str., Rositz"
  ]
  node [
    id 91
    label "Lindenthal Lindenallee, Leipzig"
  ]
  node [
    id 92
    label "GVZ Nord T&#38;S, Leipzig"
  ]
  node [
    id 93
    label "Haertelstr., Leipzig"
  ]
  node [
    id 94
    label "Merseburg Bahnhof"
  ]
  node [
    id 95
    label "Altscherbitz, Schkeuditz"
  ]
  node [
    id 96
    label "Wiederitzsch-Mitte, Leipzig"
  ]
  node [
    id 97
    label "Permoser-/Hohentichelnstr., Leipzig"
  ]
  node [
    id 98
    label "Busplatz, Meuselwitz"
  ]
  node [
    id 99
    label "Engelsdorf Gueterbahnhofstr., Leipzig"
  ]
  node [
    id 100
    label "Grosspoetzschau Eiche, Espenhain"
  ]
  node [
    id 101
    label "Auensee, Leipzig"
  ]
  node [
    id 102
    label "Leipziger Str., Borsdorf"
  ]
  node [
    id 103
    label "Kleindalzig, Zwenkau"
  ]
  node [
    id 104
    label "Plaussig Grundstr., Leipzig"
  ]
  node [
    id 105
    label "Lindenhayn, Schoenwoelkau"
  ]
  node [
    id 106
    label "Friedrichshafner Str., Leipzig"
  ]
  node [
    id 107
    label "Sittel, Kitzen"
  ]
  node [
    id 108
    label "Nossen Bahnhof"
  ]
  node [
    id 109
    label "Wilhelm-Liebknecht-Platz, Leipzig"
  ]
  node [
    id 110
    label "Arthur-Nagel-Str., Leipzig"
  ]
  node [
    id 111
    label "Ost Schillerplatz, Markkleeberg"
  ]
  node [
    id 112
    label "Franz-Mehring-Str., Leipzig"
  ]
  node [
    id 113
    label "Lindenthal Gemeindeamt, Leipzig"
  ]
  node [
    id 114
    label "Breitenfeld Tierheim, Leipzig"
  ]
  node [
    id 115
    label "Raiffeisenstr., Leipzig"
  ]
  node [
    id 116
    label "Dreiecksweg, Leipzig"
  ]
  node [
    id 117
    label "Komarowstr., Leipzig"
  ]
  node [
    id 118
    label "Fortunabadstr., Leipzig"
  ]
  node [
    id 119
    label "Beyerleinstr., Leipzig"
  ]
  node [
    id 120
    label "Wellaune B2, Bad Dueben"
  ]
  node [
    id 121
    label "Panitzsch Schule, Borsdorf"
  ]
  node [
    id 122
    label "Wilhelm-Leuschner-Platz, Leipzig"
  ]
  node [
    id 123
    label "Doelzig B181/Suedstr., Schkeuditz"
  ]
  node [
    id 124
    label "Doelzig Feldstr., Schkeuditz"
  ]
  node [
    id 125
    label "Obergruna B101, Grossschirma"
  ]
  node [
    id 126
    label "Threna Abzw. nach Fuchshain, Belgershain"
  ]
  node [
    id 127
    label "Witzgallstr., Leipzig"
  ]
  node [
    id 128
    label "Marschnerstr., Leipzig"
  ]
  node [
    id 129
    label "Wurzener Str., Naunhof"
  ]
  node [
    id 130
    label "Stallbaumstr., Leipzig"
  ]
  node [
    id 131
    label "Parkallee, Leipzig"
  ]
  node [
    id 132
    label "Grosszschocher Anton-Zickmantel-Str., Leipzig"
  ]
  node [
    id 133
    label "Penig Markt"
  ]
  node [
    id 134
    label "Duerrstr., Leipzig"
  ]
  node [
    id 135
    label "Fuchshain Kirche, Naunhof"
  ]
  node [
    id 136
    label "Moelkau An den Platanen, Leipzig"
  ]
  node [
    id 137
    label "Portitzer Allee S-Bahnhof Heiterblick, Leipzig"
  ]
  node [
    id 138
    label "Guenther-Platz, Hartmannsdorf b Chemnitz (Sachsen)"
  ]
  node [
    id 139
    label "Max-Lingner-Str., Leipzig"
  ]
  node [
    id 140
    label "Panitzsch Sommerfelder Str., Borsdorf"
  ]
  node [
    id 141
    label "Str. am Park, Leipzig"
  ]
  node [
    id 142
    label "Weimarer Str., Leipzig"
  ]
  node [
    id 143
    label "Gh Drei Lilien, Penig"
  ]
  node [
    id 144
    label "Boehlitz-Ehrenberg Barnecker Str., Leipzig"
  ]
  node [
    id 145
    label "Moelkau Gottschalkstr., Leipzig"
  ]
  node [
    id 146
    label "Wachau Gewerbegebiet, Markkleeberg"
  ]
  node [
    id 147
    label "Bayerischer Bahnhof, Leipzig"
  ]
  node [
    id 148
    label "S-Bahnhof Anger-Crottendorf, Leipzig"
  ]
  node [
    id 149
    label "Koehra Leipziger Str., Belgershain"
  ]
  node [
    id 150
    label "Kurt-Eisner-Str./Arthur-Hoffmann-Str., Leipzig"
  ]
  node [
    id 151
    label "Deutsche Nationalbibliothek, Leipzig"
  ]
  node [
    id 152
    label "Groepplerstr., Leipzig"
  ]
  node [
    id 153
    label "Landsberger Str./Max-Liebermann-Str., Leipzig"
  ]
  node [
    id 154
    label "Weinbergstr., Leipzig"
  ]
  node [
    id 155
    label "Thesau, Kitzen"
  ]
  node [
    id 156
    label "GVZ Nord Porschestr., Leipzig"
  ]
  node [
    id 157
    label "Arthur-Hoffmann-Str./Richard-Lehmann-Str., Leipzig"
  ]
  node [
    id 158
    label "August-Bebel-Str./Richard-Lehmann-Str., Leipzig"
  ]
  node [
    id 159
    label "Oberdorfstr., Leipzig"
  ]
  node [
    id 160
    label "Grosszschocher Gerhard-Ellrodt-Str., Leipzig"
  ]
  node [
    id 161
    label "Liebertwolkwitz Rossstr., Leipzig"
  ]
  node [
    id 162
    label "Ratzelbogen, Leipzig"
  ]
  node [
    id 163
    label "Theresienstr., Leipzig"
  ]
  node [
    id 164
    label "Paetzstr., Schkeuditz"
  ]
  node [
    id 165
    label "Waldplatz, Leipzig"
  ]
  node [
    id 166
    label "Hohentichelnstr., Leipzig"
  ]
  node [
    id 167
    label "Liebertwolkwitz An der Brauerei, Leipzig"
  ]
  node [
    id 168
    label "Gut Seehausen, Leipzig"
  ]
  node [
    id 169
    label "Frankenheim Friedenseiche, Markranstaedt"
  ]
  node [
    id 170
    label "Audigast, Groitzsch b Pegau"
  ]
  node [
    id 171
    label "Gerichshain Leipziger Str., Machern"
  ]
  node [
    id 172
    label "Waldkrankenhaus, Bad Dueben"
  ]
  node [
    id 173
    label "Dr.-Kuelz-Ring, Eilenburg"
  ]
  node [
    id 174
    label "Am kleinen Feld, Leipzig"
  ]
  node [
    id 175
    label "Weidenweg, Leipzig"
  ]
  node [
    id 176
    label "Moelkau Am Bahndamm, Leipzig"
  ]
  node [
    id 177
    label "Liebigstr., Leipzig"
  ]
  node [
    id 178
    label "Hohenossig Abzw. Zschoelkau, Krostitz"
  ]
  node [
    id 179
    label "Freiburger Allee, Markkleeberg"
  ]
  node [
    id 180
    label "Baalsdorf Brandiser Str., Leipzig"
  ]
  node [
    id 181
    label "Plaussig Am Kellerberg, Leipzig"
  ]
  node [
    id 182
    label "Gohlis Landsberger Str., Leipzig"
  ]
  node [
    id 183
    label "Borsdorf S-Bahnhof"
  ]
  node [
    id 184
    label "Am Anger, Markranstaedt"
  ]
  node [
    id 185
    label "Sachsenklinik, Naunhof-Erdmannshain"
  ]
  node [
    id 186
    label "Rennbahn, Leipzig"
  ]
  node [
    id 187
    label "Kreypau Abzw., Merseburg"
  ]
  node [
    id 188
    label "Hoeltystr., Leipzig"
  ]
  node [
    id 189
    label "Martinshoehe, Leipzig"
  ]
  node [
    id 190
    label "Knauthain Am Klucksgraben, Leipzig"
  ]
  node [
    id 191
    label "Lausen Wolkenweg, Leipzig"
  ]
  node [
    id 192
    label "Nicolaiplatz, Grimma"
  ]
  node [
    id 193
    label "Engelsdorf Arnoldplatz, Leipzig"
  ]
  node [
    id 194
    label "Waldsiedlung, Markranstaedt-Goehrenz"
  ]
  node [
    id 195
    label "Essener/Friedrichshafner Str., Leipzig"
  ]
  node [
    id 196
    label "Gaschwitz Am Park, Markkleeberg"
  ]
  node [
    id 197
    label "Panitzsch Dreiecksiedlung, Borsdorf"
  ]
  node [
    id 198
    label "Kurt-Kresse-Str., Leipzig"
  ]
  node [
    id 199
    label "Lipsiusstr., Leipzig"
  ]
  node [
    id 200
    label "Lindenau Friedhof, Leipzig"
  ]
  node [
    id 201
    label "Krostitz"
  ]
  node [
    id 202
    label "Porsche/Suedtor, Leipzig"
  ]
  node [
    id 203
    label "Luetzschena (Tram), Leipzig"
  ]
  node [
    id 204
    label "Penig Bahnhof"
  ]
  node [
    id 205
    label "Thekla Tauchaer Str., Leipzig"
  ]
  node [
    id 206
    label "Lausen Lausener Bogen, Leipzig"
  ]
  node [
    id 207
    label "Zentrum, Rositz"
  ]
  node [
    id 208
    label "Koernerstr., Leipzig"
  ]
  node [
    id 209
    label "Auensiedlung, Grosspoesna"
  ]
  node [
    id 210
    label "Doelzig Kirche, Schkeuditz"
  ]
  node [
    id 211
    label "Schulstr., Hartmannsdorf b Chemnitz (Sachsen)"
  ]
  node [
    id 212
    label "Leipziger Str., Grimma"
  ]
  node [
    id 213
    label "Grossdeubener Weg, Zwenkau"
  ]
  node [
    id 214
    label "Boehlitz-Ehrenberg Burghausener Str., Leipzig"
  ]
  node [
    id 215
    label "Zweenfurth Grosser Weg, Borsdorf"
  ]
  node [
    id 216
    label "Unterer Bahnhof, Delitzsch"
  ]
  node [
    id 217
    label "Zschoechergen Abzw., Koetschlitz"
  ]
  node [
    id 218
    label "Kesselshain/B95, Borna"
  ]
  node [
    id 219
    label "Leipziger Str., Zwochau"
  ]
  node [
    id 220
    label "Thekla S-Bahnhof, Leipzig"
  ]
  node [
    id 221
    label "Karlstr., Delitzsch"
  ]
  node [
    id 222
    label "Klebendorfer Str., Taucha (Sachsen)"
  ]
  node [
    id 223
    label "Plaussig Gewerbegebiet, Leipzig"
  ]
  node [
    id 224
    label "Neuschladitz, Rackwitz"
  ]
  node [
    id 225
    label "Selliner Str. Gesundheitszentrum, Leipzig"
  ]
  node [
    id 226
    label "Holzhausen Hauptstr., Leipzig"
  ]
  node [
    id 227
    label "Lindenau Nathanaelkirche, Leipzig"
  ]
  node [
    id 228
    label "Hildebrandstr., Leipzig"
  ]
  node [
    id 229
    label "Gutenbergplatz, Leipzig"
  ]
  node [
    id 230
    label "Bautzner Str./Baestleinstr., Leipzig"
  ]
  node [
    id 231
    label "Zoebigker Holunderweg, Markkleeberg"
  ]
  node [
    id 232
    label "Moelkau Lintacherstr., Leipzig"
  ]
  node [
    id 233
    label "Groitzsch b Pegau Markt"
  ]
  node [
    id 234
    label "Modelwitz, Schkeuditz"
  ]
  node [
    id 235
    label "Volksgartenstrasse, Leipzig"
  ]
  node [
    id 236
    label "Kirchenheim, Taucha (Sachsen) Sehlis"
  ]
  node [
    id 237
    label "Wiederitzscher Str., Rackwitz-Podelwitz"
  ]
  node [
    id 238
    label "Plaussiger Dorfstrasse, Leipzig"
  ]
  node [
    id 239
    label "Arno-Nitzsche-Str./Arthur-Hoffmann-Str., Leipzig"
  ]
  node [
    id 240
    label "Leonhard-Frank-Str., Leipzig"
  ]
  node [
    id 241
    label "Steinstrasse (Aerztehaus Sued), Leipzig"
  ]
  node [
    id 242
    label "Siedlung Florian Geyer, Leipzig"
  ]
  node [
    id 243
    label "Tresenweg, Taucha (Sachsen) Sehlis"
  ]
  node [
    id 244
    label "Kobschuetz, Groitzsch b Pegau"
  ]
  node [
    id 245
    label "Clara-Zetkin-Platz, Wurzen"
  ]
  node [
    id 246
    label "Grimma Bahnhof"
  ]
  node [
    id 247
    label "Kunzestr., Leipzig"
  ]
  node [
    id 248
    label "Obergruna Abzw. Bergmann, Grossschirma"
  ]
  node [
    id 249
    label "Schulstr., Markkleeberg"
  ]
  node [
    id 250
    label "Cradefelder Str., Leipzig"
  ]
  node [
    id 251
    label "Frankenheim Gewerbegebiet, Markranstaedt"
  ]
  node [
    id 252
    label "Haenichen Bismarckturm, Leipzig"
  ]
  node [
    id 253
    label "Zitzschen Teich, Zwenkau"
  ]
  node [
    id 254
    label "Wiederitzsch Buergeramt, Leipzig"
  ]
  node [
    id 255
    label "Leibnizstr., Leipzig"
  ]
  node [
    id 256
    label "Lauerscher Weg, Leipzig"
  ]
  node [
    id 257
    label "Lessingstr., Delitzsch"
  ]
  node [
    id 258
    label "Goerdelerring, Leipzig"
  ]
  node [
    id 259
    label "Paunsdorfer Allee/Permoser Str., Leipzig"
  ]
  node [
    id 260
    label "Panitzsch Kindergarten, Borsdorf"
  ]
  node [
    id 261
    label "Zentralsportpark, Markkleeberg"
  ]
  node [
    id 262
    label "Landratsamt, Delitzsch"
  ]
  node [
    id 263
    label "Brodenaundorf, Zschortau"
  ]
  node [
    id 264
    label "Chausseehaus, Leipzig"
  ]
  node [
    id 265
    label "Holzhausen Moelkauer Str., Leipzig"
  ]
  node [
    id 266
    label "Stuttgarter Allee, Leipzig"
  ]
  node [
    id 267
    label "Freiligrathstr., Taucha (Sachsen)"
  ]
  node [
    id 268
    label "Albersdorfer Str., Markranstaedt-Goehrenz"
  ]
  node [
    id 269
    label "Doelzig Frankenheimer Str., Schkeuditz"
  ]
  node [
    id 270
    label "Schoenau Kirche, Leipzig"
  ]
  node [
    id 271
    label "Kanalstr., Altenburg"
  ]
  node [
    id 272
    label "Am Obstgut, Taucha (Sachsen)"
  ]
  node [
    id 273
    label "Kleinwaltersdorf Am Forsthaus, Freiberg (Sachsen)"
  ]
  node [
    id 274
    label "Pomssen Hauptstr., Parthenstein"
  ]
  node [
    id 275
    label "Grosspriessligk B176, Groitzsch b Pegau"
  ]
  node [
    id 276
    label "Hohe Str., Leipzig"
  ]
  node [
    id 277
    label "Str. am See, Leipzig"
  ]
  node [
    id 278
    label "An der Schachtbahn, Markranstaedt"
  ]
  node [
    id 279
    label "Roseggerstr., Leipzig"
  ]
  node [
    id 280
    label "Richard-Lehmann-Str./Zwickauer Str., Leipzig"
  ]
  node [
    id 281
    label "Henriettenstr., Leipzig"
  ]
  node [
    id 282
    label "Mehringstr., Markkleeberg"
  ]
  node [
    id 283
    label "Paunsdorf Nord, Leipzig"
  ]
  node [
    id 284
    label "Annaberger Str., Leipzig"
  ]
  node [
    id 285
    label "Moelkau Gemeindeamt, Leipzig"
  ]
  node [
    id 286
    label "Dr.-Hermann-Duncker-Str., Leipzig"
  ]
  node [
    id 287
    label "Ernst-Keil-Str., Leipzig"
  ]
  node [
    id 288
    label "Luckaer Str., Meuselwitz"
  ]
  node [
    id 289
    label "Werben Teich, Kitzen"
  ]
  node [
    id 290
    label "Gartencenter, Delitzsch"
  ]
  node [
    id 291
    label "Antonien-/Giesserstr., Leipzig"
  ]
  node [
    id 292
    label "Bergschaenke, Wallendorf (Luppe)"
  ]
  node [
    id 293
    label "Sternsiedlung Nord, Leipzig"
  ]
  node [
    id 294
    label "Am Kirschberg, Leipzig"
  ]
  node [
    id 295
    label "Struempellstr., Leipzig"
  ]
  node [
    id 296
    label "Industriegebiet, Espenhain"
  ]
  node [
    id 297
    label "Panitzsch Abzw. Cunnersdorf, Borsdorf"
  ]
  node [
    id 298
    label "Binzer Str., Leipzig"
  ]
  node [
    id 299
    label "Hirschfelder Str., Borsdorf-Zweenfurth"
  ]
  node [
    id 300
    label "Virchowstr./Coppistr., Leipzig"
  ]
  node [
    id 301
    label "Hohenossig, Krostitz"
  ]
  node [
    id 302
    label "Zoebigker An der Harth, Markkleeberg"
  ]
  node [
    id 303
    label "Bergstr., Leipzig"
  ]
  node [
    id 304
    label "Klempererstr., Leipzig"
  ]
  node [
    id 305
    label "Moelkau Moelkauer Dorfplatz, Leipzig"
  ]
  node [
    id 306
    label "Althener Anger, Leipzig"
  ]
  node [
    id 307
    label "Amtshaeuser, Merseburg"
  ]
  node [
    id 308
    label "Kommandant-Prendel-Allee, Leipzig"
  ]
  node [
    id 309
    label "Hirschfeld, Leipzig"
  ]
  node [
    id 310
    label "Gruenauer Allee (Bus/Tram), Leipzig"
  ]
  node [
    id 311
    label "Rosenowstr., Leipzig"
  ]
  node [
    id 312
    label "Seenallee, Markkleeberg"
  ]
  node [
    id 313
    label "Zoebigker Schmiede, Markkleeberg"
  ]
  node [
    id 314
    label "Pahlenweg, Leipzig"
  ]
  node [
    id 315
    label "Suedplatz, Leipzig"
  ]
  node [
    id 316
    label "Naunhof Markt"
  ]
  node [
    id 317
    label "Albersdorfer Str., Leipzig"
  ]
  node [
    id 318
    label "Gustav-Raute-Str., Eilenburg"
  ]
  node [
    id 319
    label "Rueckmarsdorf Wachbergallee, Leipzig"
  ]
  node [
    id 320
    label "Hohendorf, Groitzsch b Pegau"
  ]
  node [
    id 321
    label "Am Bahnhof, Zwenkau"
  ]
  node [
    id 322
    label "Engelsdorf Artur-Winkler-Str., Leipzig"
  ]
  node [
    id 323
    label "Luetzschena Freirodaer Weg, Leipzig"
  ]
  node [
    id 324
    label "Wiederitzscher Str., Leipzig"
  ]
  node [
    id 325
    label "Autobahn, Wiedemar"
  ]
  node [
    id 326
    label "Kiewer Str./Kaufland, Leipzig"
  ]
  node [
    id 327
    label "Elsastr., Leipzig"
  ]
  node [
    id 328
    label "Elisabeth-Schumacher-Str., Leipzig"
  ]
  node [
    id 329
    label "Reinhardtstr., Leipzig"
  ]
  node [
    id 330
    label "Wachau Hotel Atlanta, Markkleeberg"
  ]
  node [
    id 331
    label "Zoeschen"
  ]
  node [
    id 332
    label "Zuckerraffinerie, Rositz"
  ]
  node [
    id 333
    label "Gustav-Freytag-Str., Leipzig"
  ]
  node [
    id 334
    label "Schwartzestr., Leipzig"
  ]
  node [
    id 335
    label "Boehlitz-Ehrenberg Forstweg, Leipzig"
  ]
  node [
    id 336
    label "Nordring, Eilenburg"
  ]
  node [
    id 337
    label "Liebertwolkwitz Schwarzes Ross, Leipzig"
  ]
  node [
    id 338
    label "Neutzscher Str., Leipzig"
  ]
  node [
    id 339
    label "Davidstr., Taucha (Sachsen)"
  ]
  node [
    id 340
    label "Stoermthal Nord, Grosspoesna"
  ]
  node [
    id 341
    label "Merkwitz, Taucha (Sachsen)"
  ]
  node [
    id 342
    label "Hartmannsdorf Gasthof, Leipzig"
  ]
  node [
    id 343
    label "Otto-Schmidt-Str., Taucha (Sachsen)"
  ]
  node [
    id 344
    label "Engelsdorf Ernst-Guhr-Str., Leipzig"
  ]
  node [
    id 345
    label "Kriebitzsch B180"
  ]
  node [
    id 346
    label "Messeverwaltung, Leipzig"
  ]
  node [
    id 347
    label "Albrechtshain Dorfstr., Naunhof"
  ]
  node [
    id 348
    label "Holzhausen Sophienhoehe, Leipzig"
  ]
  node [
    id 349
    label "Rosa-Luxemburg-Str., Eilenburg"
  ]
  node [
    id 350
    label "Felsenkeller, Leipzig"
  ]
  node [
    id 351
    label "Altenburg Bahnhof"
  ]
  node [
    id 352
    label "Gaernitz Gasthof, Markranstaedt"
  ]
  node [
    id 353
    label "Future Electronics, Leipzig"
  ]
  node [
    id 354
    label "Leonhardtstr., Leipzig"
  ]
  node [
    id 355
    label "Schnaudertrebnitz, Groitzsch b Pegau"
  ]
  node [
    id 356
    label "Mozartstr. (Grassistr.), Leipzig"
  ]
  node [
    id 357
    label "Markkleeberg West Wasserturmstr."
  ]
  node [
    id 358
    label "Markkleeberg Ost Auenplatz"
  ]
  node [
    id 359
    label "Grossstaedteln S-Bahnhof, Markkleeberg"
  ]
  node [
    id 360
    label "Torgauer Landstr., Eilenburg"
  ]
  node [
    id 361
    label "Abzw. Damaschkestr., Naunhof"
  ]
  node [
    id 362
    label "Cospudener See Erlebnisachse, Leipzig"
  ]
  node [
    id 363
    label "Zeitzer Str./Mehringstr., Altenburg"
  ]
  node [
    id 364
    label "Roedelstr., Leipzig"
  ]
  node [
    id 365
    label "Lindenthal Zeisigweg, Leipzig"
  ]
  node [
    id 366
    label "Paradeplatz, Bad Dueben"
  ]
  node [
    id 367
    label "Roehrsdorf Chemnitz Center, Chemnitz"
  ]
  node [
    id 368
    label "Brauteich, Grosspoesna"
  ]
  node [
    id 369
    label "Heisenbergstr., Leipzig"
  ]
  node [
    id 370
    label "Siedlerweg, Leipzig"
  ]
  node [
    id 371
    label "Wiederitzsch Wiederitzscher Landstr., Leipzig"
  ]
  node [
    id 372
    label "Am Rittergut, Zschortau-Lemsel"
  ]
  node [
    id 373
    label "Friedrich-List-Platz, Leipzig"
  ]
  node [
    id 374
    label "Sellerhausen, Leipzig"
  ]
  node [
    id 375
    label "Freiberger Str., Leipzig"
  ]
  node [
    id 376
    label "BMW Presswerk, Leipzig"
  ]
  node [
    id 377
    label "Doelitz Am Eichwinkel, Leipzig"
  ]
  node [
    id 378
    label "Feldstr., Leipzig"
  ]
  node [
    id 379
    label "Schulstr., Zwenkau"
  ]
  node [
    id 380
    label "Rueckmarsdorf Einkaufspassage, Leipzig"
  ]
  node [
    id 381
    label "Hofmeisterstr., Leipzig"
  ]
  node [
    id 382
    label "Teslastr., Leipzig"
  ]
  node [
    id 383
    label "Kitzen Siedlung"
  ]
  node [
    id 384
    label "Martin-Herrmann-Str., Leipzig"
  ]
  node [
    id 385
    label "Grossmiltitzer Str., Leipzig"
  ]
  node [
    id 386
    label "Wahren (Rathaus), Leipzig"
  ]
  node [
    id 387
    label "Hauptbahnhof/Goethestr., Leipzig"
  ]
  node [
    id 388
    label "Holzhausen Walter-Heise-Str., Leipzig"
  ]
  node [
    id 389
    label "Georg-Schumann-Str./Lindenthaler Str., Leipzig"
  ]
  node [
    id 390
    label "Melissenweg, Leipzig"
  ]
  node [
    id 391
    label "Paul-Michael-Str., Leipzig"
  ]
  node [
    id 392
    label "Rathausplatz, Schkeuditz"
  ]
  node [
    id 393
    label "Lehelitz B2, Krostitz"
  ]
  node [
    id 394
    label "Gohlis-Nord, Leipzig"
  ]
  node [
    id 395
    label "Gottschedstr., Leipzig"
  ]
  node [
    id 396
    label "Seehausen Seehausener Allee, Leipzig"
  ]
  node [
    id 397
    label "Gottscheina Am Ring, Leipzig"
  ]
  node [
    id 398
    label "Zoo, Leipzig"
  ]
  node [
    id 399
    label "Handwerkerhof, Leipzig"
  ]
  node [
    id 400
    label "Bremer Str., Leipzig"
  ]
  node [
    id 401
    label "Podelwitz Gaststaette, Rackwitz"
  ]
  node [
    id 402
    label "Schwantesstr., Leipzig"
  ]
  node [
    id 403
    label "Oellschuetz, Groitzsch b Pegau"
  ]
  node [
    id 404
    label "Engelsdorf Kirchweg, Leipzig"
  ]
  node [
    id 405
    label "Hauptbahnhof Westseite, Leipzig"
  ]
  node [
    id 406
    label "Paunsdorfer Str., Leipzig"
  ]
  node [
    id 407
    label "Drosskau, Groitzsch b Pegau"
  ]
  node [
    id 408
    label "Rinckartstr./Markt, Eilenburg"
  ]
  node [
    id 409
    label "Lindenthal An der Schule, Leipzig"
  ]
  node [
    id 410
    label "Wenceslaikirche, Wurzen"
  ]
  node [
    id 411
    label "Wasserwerk Windorf, Leipzig"
  ]
  node [
    id 412
    label "Baalsdorf Baalsdorfer Str., Leipzig"
  ]
  node [
    id 413
    label "Landsberger/Bahnhofstr., Leipzig"
  ]
  node [
    id 414
    label "Wilhelminenstr., Leipzig"
  ]
  node [
    id 415
    label "Radefeld Koelner Str., Schkeuditz"
  ]
  node [
    id 416
    label "Lindenthal Sophienstr., Leipzig"
  ]
  node [
    id 417
    label "Schkeitbar, Markranstaedt"
  ]
  node [
    id 418
    label "Grosszschocher Wasserturmweg, Leipzig"
  ]
  node [
    id 419
    label "Arthur-Polenz-Str., Leipzig"
  ]
  node [
    id 420
    label "Fuchshain Feldstr., Naunhof"
  ]
  node [
    id 421
    label "Hauptstr., Wiedemar"
  ]
  node [
    id 422
    label "Borna B93/Klinikum"
  ]
  node [
    id 423
    label "Suedfriedhof, Leipzig"
  ]
  node [
    id 424
    label "Stoermthal Gewerbegebiet, Grosspoesna"
  ]
  node [
    id 425
    label "Gruenau-Nord, Leipzig"
  ]
  node [
    id 426
    label "BMW Werk Tor 1, Leipzig"
  ]
  node [
    id 427
    label "Knauthain Abzw. n. Hartmannsdorf, Leipzig"
  ]
  node [
    id 428
    label "Boehlitz-Ehrenberg Heinrich-Heine-Str., Leipzig"
  ]
  node [
    id 429
    label "Suedring, Kitzen-Werben"
  ]
  node [
    id 430
    label "Sehliser Str., Borsdorf-Panitzsch"
  ]
  node [
    id 431
    label "Einfahrt Sachsenpark, Leipzig"
  ]
  node [
    id 432
    label "Plaut-/Schomburgkstr., Leipzig"
  ]
  node [
    id 433
    label "Threna Grimmaer Str., Belgershain"
  ]
  node [
    id 434
    label "Gaertnerstr., Leipzig"
  ]
  node [
    id 435
    label "Holzhausen Colmberg-Siedlung, Leipzig"
  ]
  node [
    id 436
    label "Uranusstr., Leipzig"
  ]
  node [
    id 437
    label "Leipzig Markt (Bus)"
  ]
  node [
    id 438
    label "Wurzener Platz, Eilenburg"
  ]
  node [
    id 439
    label "Diakonissenhaus, Leipzig"
  ]
  node [
    id 440
    label "Altenburger Str., Leipzig"
  ]
  node [
    id 441
    label "Sommerfeld, Leipzig"
  ]
  node [
    id 442
    label "Wellaune Rotes Haus, Bad Dueben"
  ]
  node [
    id 443
    label "Matthesstr., Chemnitz"
  ]
  node [
    id 444
    label "Wilhelmstr., Leipzig-Lindenthal"
  ]
  node [
    id 445
    label "Busplatz, Lucka"
  ]
  node [
    id 446
    label "Hartmannsdorf b Chemnitz (Sachsen) Feuerwehr"
  ]
  node [
    id 447
    label "Gerichtsweg, Leipzig"
  ]
  node [
    id 448
    label "Loeben Teich, Kitzen"
  ]
  node [
    id 449
    label "Gottscheina Siedlung, Leipzig"
  ]
  node [
    id 450
    label "Schenkendorfstr., Leipzig"
  ]
  node [
    id 451
    label "Stoehrer/Braunstr., Leipzig"
  ]
  node [
    id 452
    label "Reichsstr., Leipzig"
  ]
  node [
    id 453
    label "Grebehna, Zwochau"
  ]
  node [
    id 454
    label "Miltitz Friedhof, Leipzig"
  ]
  node [
    id 455
    label "Riebeckstr./Oststr., Leipzig"
  ]
  node [
    id 456
    label "Stuenz, Leipzig"
  ]
  node [
    id 457
    label "Erdmannshain Kirche, Naunhof"
  ]
  node [
    id 458
    label "An den Pferdnerkabeln, Leipzig"
  ]
  node [
    id 459
    label "Roehrsdorf Querstr., Chemnitz"
  ]
  node [
    id 460
    label "Mathildenstr., Leipzig"
  ]
  node [
    id 461
    label "Gestewitz Ort, Borna"
  ]
  node [
    id 462
    label "Chemnitzer/Reitzenhainer Str., Penig"
  ]
  node [
    id 463
    label "Brandstr./Selnecker Str., Leipzig"
  ]
  node [
    id 464
    label "Holzhausen Saechsisches Haus, Leipzig"
  ]
  node [
    id 465
    label "Schoenefeld Volbedingstr., Leipzig"
  ]
  node [
    id 466
    label "Lehrwerkstatt, Meuselwitz"
  ]
  node [
    id 467
    label "Dantestr., Leipzig"
  ]
  node [
    id 468
    label "Coppiplatz S-Bahnhof, Leipzig"
  ]
  node [
    id 469
    label "Naunhofer Str., Leipzig"
  ]
  node [
    id 470
    label "Auenhain Kanupark, Markkleeberg"
  ]
  node [
    id 471
    label "Grethen Leipziger Str., Parthenstein"
  ]
  node [
    id 472
    label "Connewitz Kreuz, Leipzig"
  ]
  node [
    id 473
    label "Woelpern Haltestelle, Jesewitz"
  ]
  node [
    id 474
    label "Kulkwitz Kraftwerk, Markranstaedt"
  ]
  node [
    id 475
    label "Loedla"
  ]
  node [
    id 476
    label "Kleinpoetzschau, Espenhain"
  ]
  node [
    id 477
    label "Bautzner Str., Leipzig"
  ]
  node [
    id 478
    label "Hohenheida Gasthof, Leipzig"
  ]
  node [
    id 479
    label "Raepitz Am Bahnhof, Markranstaedt"
  ]
  node [
    id 480
    label "Biokraftwerk, Delitzsch"
  ]
  node [
    id 481
    label "Wahren (Am Zuckmantel/Fr.-Bosse-Str.), Leipzig"
  ]
  node [
    id 482
    label "Pommernstr., Leipzig"
  ]
  node [
    id 483
    label "Alter Gasthof, Grosspoesna"
  ]
  node [
    id 484
    label "Lindenau Bushof, Leipzig"
  ]
  node [
    id 485
    label "Breitenfeld Kutscherweg, Leipzig"
  ]
  node [
    id 486
    label "Plovdiver Str., Leipzig"
  ]
  node [
    id 487
    label "Schoenau Weissdornstr., Leipzig"
  ]
  node [
    id 488
    label "Herrmann-Meyer-Str., Leipzig"
  ]
  node [
    id 489
    label "Ringstr., Leipzig"
  ]
  node [
    id 490
    label "Markkleeberg S-Bahnhof"
  ]
  node [
    id 491
    label "Radefeld Friedhof, Schkeuditz"
  ]
  node [
    id 492
    label "Zollschuppenst., Leipzig"
  ]
  node [
    id 493
    label "Georg-Schwarz-Str./Merseburger Str., Leipzig"
  ]
  node [
    id 494
    label "Ostsiedlung, Markranstaedt"
  ]
  node [
    id 495
    label "Knauthain Schoenbergstr., Leipzig"
  ]
  node [
    id 496
    label "Leupoldstr., Leipzig"
  ]
  node [
    id 497
    label "Sportplatz Grassdorf, Taucha (Sachsen)"
  ]
  node [
    id 498
    label "Hedwigstr., Taucha (Sachsen) Ploesitz"
  ]
  node [
    id 499
    label "Radefeld Milanstr., Schkeuditz"
  ]
  node [
    id 500
    label "Portitz Teichsiedlung, Leipzig"
  ]
  node [
    id 501
    label "Gerichshainer Str., Borsdorf-Panitzsch"
  ]
  node [
    id 502
    label "Mosenthinstr., Leipzig"
  ]
  node [
    id 503
    label "Jupiterstr., Leipzig"
  ]
  node [
    id 504
    label "Wahren S-Bahnhof, Leipzig"
  ]
  node [
    id 505
    label "Scheidens, Kitzen"
  ]
  node [
    id 506
    label "S-Bahnhof Plagwitz, Leipzig"
  ]
  node [
    id 507
    label "Kriebitzsch Kreuzung"
  ]
  node [
    id 508
    label "Am Veitsberg, Taucha (Sachsen)"
  ]
  node [
    id 509
    label "Dresdener Landstr., Borsdorf"
  ]
  node [
    id 510
    label "Strasse nach Althen, Borsdorf"
  ]
  node [
    id 511
    label "Karl-Liebknecht-Str./Kurt-Eisner-Str., Leipzig"
  ]
  node [
    id 512
    label "Zechau Ost, Rositz"
  ]
  node [
    id 513
    label "Suedvorstadt Loessniger Str., Leipzig"
  ]
  node [
    id 514
    label "Russenstr., Leipzig"
  ]
  node [
    id 515
    label "August-Bebel-Str., Delitzsch"
  ]
  node [
    id 516
    label "Probstheida, Leipzig"
  ]
  node [
    id 517
    label "Schlossplatz, Freiberg (Sachsen)"
  ]
  node [
    id 518
    label "Anker, Wallendorf (Luppe)"
  ]
  node [
    id 519
    label "Augustusberg Gh Motorrast, Nossen"
  ]
  node [
    id 520
    label "Diezmannstr., Leipzig"
  ]
  node [
    id 521
    label "Ossietzky-/Gorkistrasse, Leipzig"
  ]
  node [
    id 522
    label "Samuel-Lampel-Str., Leipzig"
  ]
  node [
    id 523
    label "Rueckmarsdorf Loewencenter, Leipzig"
  ]
  node [
    id 524
    label "Muehlau Ortsmitte"
  ]
  node [
    id 525
    label "Ostheimstrasse, Leipzig"
  ]
  node [
    id 526
    label "Borna Bahnhof"
  ]
  node [
    id 527
    label "Schoenefelder Str., Leipzig"
  ]
  node [
    id 528
    label "Heide Spa, Bad Dueben"
  ]
  node [
    id 529
    label "Bruenner Str., Leipzig"
  ]
  node [
    id 530
    label "Westplatz, Leipzig"
  ]
  node [
    id 531
    label "Louis-Fuernberg-Str., Leipzig"
  ]
  node [
    id 532
    label "Schwarzenbergweg, Leipzig"
  ]
  node [
    id 533
    label "Waechterstr. (Grassistr.), Leipzig"
  ]
  node [
    id 534
    label "Rathaus Leutzsch, Leipzig"
  ]
  node [
    id 535
    label "Loebauer Str., Leipzig"
  ]
  node [
    id 536
    label "Goetheplatz, Leipzig"
  ]
  node [
    id 537
    label "Stahmelner Allee, Leipzig"
  ]
  node [
    id 538
    label "Demmeringstr., Leipzig"
  ]
  node [
    id 539
    label "Telemannstr., Leipzig"
  ]
  node [
    id 540
    label "S-Bahnhof Karlsruher Str., Leipzig"
  ]
  node [
    id 541
    label "Freundschaftsring, Leipzig-Althen"
  ]
  node [
    id 542
    label "Markranstaedt Bahnhof"
  ]
  node [
    id 543
    label "Neues Rathaus, Leipzig"
  ]
  node [
    id 544
    label "Knautnaundorf Gefrierzentrum, Leipzig"
  ]
  node [
    id 545
    label "Tragarth Abzw., Luppenau"
  ]
  node [
    id 546
    label "Barbarastr., Leipzig"
  ]
  node [
    id 547
    label "Am Hopfenteich, Markranstaedt"
  ]
  node [
    id 548
    label "Annenstr., Leipzig"
  ]
  node [
    id 549
    label "Theodor-Heuss-Str., Leipzig"
  ]
  node [
    id 550
    label "Plaussig Portitzmuehlweg, Leipzig"
  ]
  node [
    id 551
    label "Grossstaedteln Am Kraehenfeld, Markkleeberg"
  ]
  node [
    id 552
    label "Ploesitz Kriekauer Str., Taucha (Sachsen)"
  ]
  node [
    id 553
    label "Siebenlehn An der Halde, Grossschirma"
  ]
  node [
    id 554
    label "Rochlitzstr., Leipzig"
  ]
  node [
    id 555
    label "Martinstr., Leipzig"
  ]
  node [
    id 556
    label "Schkoelen, Markranstaedt"
  ]
  node [
    id 557
    label "Goehrenzer Str., Markranstaedt-Kulkwitz"
  ]
  node [
    id 558
    label "Giesserei Luckstr., Meuselwitz"
  ]
  node [
    id 559
    label "Wittenberger Str., Leipzig"
  ]
  node [
    id 560
    label "Kuechwaldring, Chemnitz"
  ]
  node [
    id 561
    label "Johannisallee, Leipzig"
  ]
  node [
    id 562
    label "Breisgaustr./S-Bahnhof Karlsruher Str., Leipzig"
  ]
  node [
    id 563
    label "An der Hufschmiede, Leipzig"
  ]
  node [
    id 564
    label "Engelsdorf Kirche, Leipzig"
  ]
  node [
    id 565
    label "Triftweg, Leipzig"
  ]
  node [
    id 566
    label "Am alten Flughafen, Leipzig"
  ]
  node [
    id 567
    label "Lindenthal An der alten Windmuehle, Leipzig"
  ]
  node [
    id 568
    label "Leipziger Str., Machern"
  ]
  node [
    id 569
    label "Cospudener See Nordstrand, Leipzig"
  ]
  node [
    id 570
    label "Seebenisch Am Alten Bahnhof, Markranstaedt"
  ]
  node [
    id 571
    label "Breslauer Str., Leipzig"
  ]
  node [
    id 572
    label "Baecker, Rackwitz"
  ]
  node [
    id 573
    label "Wiesenweg, Markranstaedt-Raepitz"
  ]
  node [
    id 574
    label "Robert-Schumann-Str. (Grassistr.), Leipzig"
  ]
  node [
    id 575
    label "Kolmstr., Leipzig"
  ]
  node [
    id 576
    label "Zur Waldenau, Leipzig"
  ]
  node [
    id 577
    label "Lindenthal Lange Trift, Leipzig"
  ]
  node [
    id 578
    label "Gaertnerei Grosszschocher, Leipzig"
  ]
  node [
    id 579
    label "Stieglitzstr., Leipzig"
  ]
  node [
    id 580
    label "Hermelinstr., Leipzig"
  ]
  node [
    id 581
    label "Engelsdorf Sternenstr., Leipzig"
  ]
  node [
    id 582
    label "Spenglerallee Gewerbe, Zwenkau"
  ]
  node [
    id 583
    label "Daemmstoffwerk, Delitzsch"
  ]
  node [
    id 584
    label "Rehbach, Leipzig"
  ]
  node [
    id 585
    label "Steinplatz, Leipzig"
  ]
  node [
    id 586
    label "Heinrich-Heine-Str., Markkleeberg"
  ]
  node [
    id 587
    label "S-Bahnhof Leutzsch, Leipzig"
  ]
  node [
    id 588
    label "Ruessen, Zwenkau"
  ]
  node [
    id 589
    label "Sommerlust, Zwenkau"
  ]
  node [
    id 590
    label "Pflegeheim, Penig"
  ]
  node [
    id 591
    label "Pfeffingerstr., Leipzig"
  ]
  node [
    id 592
    label "Lausen, Leipzig"
  ]
  node [
    id 593
    label "Glashuette, Altenburg"
  ]
  node [
    id 594
    label "Strassenkreuzung B101, Grossschirma"
  ]
  node [
    id 595
    label "Goldene Hoehe, Markkleeberg"
  ]
  node [
    id 596
    label "Burghausen, Leipzig"
  ]
  node [
    id 597
    label "Borna B93/Am Hochhaus"
  ]
  node [
    id 598
    label "Gerbisdorf, Schkeuditz"
  ]
  node [
    id 599
    label "Roehrsdorf Chemnitz Center Marktkauf, Chemnitz"
  ]
  node [
    id 600
    label "Am Eulenberg, Markkleeberg"
  ]
  node [
    id 601
    label "An der Maerchenwiese, Leipzig"
  ]
  node [
    id 602
    label "Nonnenweg, Leipzig"
  ]
  node [
    id 603
    label "Wiederitzsch Rietzschkegrund, Leipzig"
  ]
  node [
    id 604
    label "Omnibusbahnhof, Chemnitz"
  ]
  node [
    id 605
    label "Grossdalzig Bahnhof, Zwenkau"
  ]
  node [
    id 606
    label "Franzosenallee, Leipzig"
  ]
  node [
    id 607
    label "HTWK, Leipzig"
  ]
  node [
    id 608
    label "Moeckern Historischer Strassenbahnhof, Leipzig"
  ]
  node [
    id 609
    label "Rackwitzer Str., Leipzig"
  ]
  node [
    id 610
    label "Pittlerstr., Leipzig"
  ]
  node [
    id 611
    label "Doeringstr., Leipzig"
  ]
  node [
    id 612
    label "Arcus Park, Leipzig"
  ]
  node [
    id 613
    label "Leinestr., Leipzig"
  ]
  node [
    id 614
    label "Lindners Gut, Kitzen-Werben"
  ]
  node [
    id 615
    label "Grossstaedteln Kleine Aue, Markkleeberg"
  ]
  node [
    id 616
    label "Lissa Kattersnaundorfer Str., Neukyhna"
  ]
  node [
    id 617
    label "Philipp-Reis-Str., Leipzig"
  ]
  node [
    id 618
    label "Oelzschau Gasthof, Espenhain"
  ]
  node [
    id 619
    label "Breisgau-/Ringstr., Leipzig"
  ]
  node [
    id 620
    label "Abtnaundorf, Leipzig"
  ]
  node [
    id 621
    label "Seegel, Kitzen"
  ]
  node [
    id 622
    label "Flossplatz, Leipzig"
  ]
  node [
    id 623
    label "Grossstaedteln Meisenweg, Markkleeberg"
  ]
  node [
    id 624
    label "Zechau Gasthof, Kriebitzsch"
  ]
  node [
    id 625
    label "Torgauer Platz, Leipzig"
  ]
  node [
    id 626
    label "Rathaus Schoenefeld, Leipzig"
  ]
  node [
    id 627
    label "Bergstr., Taucha (Sachsen) Ploesitz"
  ]
  node [
    id 628
    label "Bergschloesschen, Zwenkau"
  ]
  node [
    id 629
    label "Lausen Buswendestelle, Leipzig"
  ]
  node [
    id 630
    label "Knauthain Thomas-Muentzer-Siedlung, Leipzig"
  ]
  node [
    id 631
    label "Theodor-Koerner-Str., Taucha (Sachsen)"
  ]
  node [
    id 632
    label "Knautnaundorf Werkstr., Leipzig"
  ]
  node [
    id 633
    label "Baaderstr., Leipzig"
  ]
  node [
    id 634
    label "Lausen Am Silo, Leipzig"
  ]
  node [
    id 635
    label "S-Bahnhof Connewitz, Leipzig"
  ]
  node [
    id 636
    label "Apelstrasse, Leipzig"
  ]
  node [
    id 637
    label "Gordemitz B87, Jesewitz"
  ]
  node [
    id 638
    label "Moelkau Anemonenweg, Leipzig"
  ]
  node [
    id 639
    label "Rumberg, Grimma"
  ]
  node [
    id 640
    label "Schoenauer/Ratzelstr., Leipzig"
  ]
  node [
    id 641
    label "Am Sportpark, Leipzig"
  ]
  node [
    id 642
    label "An der Tabaksmuehle, Leipzig"
  ]
  node [
    id 643
    label "Am Pegauer Tor, Borna"
  ]
  node [
    id 644
    label "Loessnig, Leipzig"
  ]
  node [
    id 645
    label "Grossdeuben Bahnhof, Boehlen b Leipzig"
  ]
  node [
    id 646
    label "Virchowstr., Markkleeberg"
  ]
  node [
    id 647
    label "Breitenfeld Gewerbegebiet, Leipzig"
  ]
  node [
    id 648
    label "Stannebeinplatz, Leipzig"
  ]
  node [
    id 649
    label "Waldemar-Goetze-Str., Leipzig"
  ]
  node [
    id 650
    label "Panitzsch B6 Dreiecksiedlung, Borsdorf"
  ]
  node [
    id 651
    label "Rueckmarsdorf Miltitzer Str., Leipzig"
  ]
  node [
    id 652
    label "Kleinzschocher, Leipzig"
  ]
  node [
    id 653
    label "Seebenisch Mitte, Markranstaedt"
  ]
  node [
    id 654
    label "August-Bebel-Str./Kurt-Eisner-Str., Leipzig"
  ]
  node [
    id 655
    label "Schweizer Bogen, Leipzig"
  ]
  node [
    id 656
    label "Geithainer Str., Leipzig"
  ]
  node [
    id 657
    label "Breitenfeld Parkring, Leipzig"
  ]
  node [
    id 658
    label "BMW Werk Zentralgebaeude, Leipzig"
  ]
  node [
    id 659
    label "Paunsdorf Friedhof, Leipzig"
  ]
  node [
    id 660
    label "Bahnhofstr., Hartmannsdorf b Chemnitz (Sachsen)"
  ]
  node [
    id 661
    label "Liebertwolkwitz Eulengraben, Leipzig"
  ]
  node [
    id 662
    label "Seifertshain, Grosspoesna"
  ]
  node [
    id 663
    label "Herzzentrum, Leipzig"
  ]
  node [
    id 664
    label "Erika-von-Brockdorff-Str., Leipzig"
  ]
  node [
    id 665
    label "Liebertwolkwitz An den Badeanlagen, Leipzig"
  ]
  node [
    id 666
    label "Permoser Str./Torgauer Str., Leipzig"
  ]
  node [
    id 667
    label "Coellnitz, Groitzsch b Pegau"
  ]
  node [
    id 668
    label "Ostfriedhof, Leipzig"
  ]
  node [
    id 669
    label "Breitscheidhof, Leipzig"
  ]
  node [
    id 670
    label "Goehren Abzw., Zweimen"
  ]
  node [
    id 671
    label "Rathenaustr., Leipzig"
  ]
  node [
    id 672
    label "Brahestr., Leipzig"
  ]
  node [
    id 673
    label "Gaschwitz Friedhof, Markkleeberg"
  ]
  node [
    id 674
    label "S-Bahnhof Slevogtstr., Leipzig"
  ]
  node [
    id 675
    label "Dr.-Wilhelm-Kuelz-Str., Leipzig"
  ]
  node [
    id 676
    label "Sonnenweg, Markkleeberg"
  ]
  node [
    id 677
    label "Alte Duebener Landstr., Leipzig"
  ]
  node [
    id 678
    label "Goehrenz Gaststaette am See, Markranstaedt"
  ]
  node [
    id 679
    label "Erich-Zeigner-Allee, Leipzig"
  ]
  node [
    id 680
    label "Reudnitz Koehlerstr., Leipzig"
  ]
  node [
    id 681
    label "Am Sonneneck, Leipzig"
  ]
  node [
    id 682
    label "Goebschelwitz, Leipzig"
  ]
  node [
    id 683
    label "Stoermthal Schaeferei, Grosspoesna"
  ]
  node [
    id 684
    label "Knautnaundorf Rundkapel, Leipzig"
  ]
  node [
    id 685
    label "Delitzsch Schwarz-Werbung, Neukyhna"
  ]
  node [
    id 686
    label "Schulze-Boysen-Str., Leipzig"
  ]
  node [
    id 687
    label "Markranstaedter Str., Leipzig"
  ]
  node [
    id 688
    label "Alte Muehle, Leipzig"
  ]
  node [
    id 689
    label "Robert-Koch-Str., Markranstaedt"
  ]
  node [
    id 690
    label "Holzhausen Kaerrnerstr., Leipzig"
  ]
  node [
    id 691
    label "Pflaumenallee, Grosspoesna"
  ]
  node [
    id 692
    label "Siedlung Waldfrieden, Leipzig"
  ]
  node [
    id 693
    label "Boehlitz-Ehrenberg Suedstr., Leipzig"
  ]
  node [
    id 694
    label "Schulstr., Kitzen"
  ]
  node [
    id 695
    label "Glesien Schafsteich, Schkeuditz"
  ]
  node [
    id 696
    label "Knautkleeberg, Leipzig"
  ]
  node [
    id 697
    label "Baalsdorf Kirchweg, Leipzig"
  ]
  node [
    id 698
    label "Baalsdorf Zaucheweg, Leipzig"
  ]
  node [
    id 699
    label "Plaussig Alte Theklaer Str., Leipzig"
  ]
  node [
    id 700
    label "Bahnhof Paunsdorf, Leipzig"
  ]
  node [
    id 701
    label "Lissa Koelsaer Str., Neukyhna"
  ]
  node [
    id 702
    label "Gohlis-Nord Virchowstr., Leipzig"
  ]
  node [
    id 703
    label "Energiestr., Markkleeberg"
  ]
  node [
    id 704
    label "Lindenthal Aeusserer Zeisigweg, Leipzig"
  ]
  node [
    id 705
    label "Am Seniorenzentrum, Markkleeberg"
  ]
  node [
    id 706
    label "Simon-Bolivar-Str., Leipzig"
  ]
  node [
    id 707
    label "Naunhof Bahnhof"
  ]
  node [
    id 708
    label "Mockauerstr./Volbedingstr., Leipzig"
  ]
  node [
    id 709
    label "Springerstr., Leipzig"
  ]
  node [
    id 710
    label "Poetzschau Abzw. nach Poetzschau, Espenhain"
  ]
  node [
    id 711
    label "Wintergartenstr./Hauptbahnhof, Leipzig"
  ]
  node [
    id 712
    label "Monarchenhuegel, Leipzig"
  ]
  node [
    id 713
    label "Kantstr., Penig"
  ]
  node [
    id 714
    label "Moelkau Karl-Friedrich-Str., Leipzig"
  ]
  node [
    id 715
    label "Gypsbergstr., Leipzig"
  ]
  node [
    id 716
    label "Technisches Rathaus, Leipzig"
  ]
  node [
    id 717
    label "Paunsdorf-Center, Leipzig"
  ]
  node [
    id 718
    label "Cleudner Str., Leipzig"
  ]
  node [
    id 719
    label "Am Viadukt, Leipzig"
  ]
  node [
    id 720
    label "Siemensstr., Leipzig"
  ]
  node [
    id 721
    label "Azaleenstr., Leipzig"
  ]
  node [
    id 722
    label "Schwarzer Baer, Guenthersdorf b Merseburg"
  ]
  node [
    id 723
    label "Lausen Regenbogen, Leipzig"
  ]
  node [
    id 724
    label "GVZ Nord Deutsche Post AG, Leipzig"
  ]
  node [
    id 725
    label "Rathaus Stoetteritz, Leipzig"
  ]
  node [
    id 726
    label "Tauscha Autoreparatur, Penig"
  ]
  node [
    id 727
    label "An der Buergerruhe, Taucha (Sachsen)"
  ]
  node [
    id 728
    label "Rossplatz/Gruenewaldstr., Leipzig"
  ]
  node [
    id 729
    label "Lindenaustr., Altenburg"
  ]
  node [
    id 730
    label "Krakauer Str., Leipzig"
  ]
  node [
    id 731
    label "S-Bahnhof Leipzig Nord, Leipzig"
  ]
  node [
    id 732
    label "Am Mueckenschloesschen, Leipzig"
  ]
  node [
    id 733
    label "Sebastian-Bach-Str., Markkleeberg"
  ]
  node [
    id 734
    label "Viertelsweg, Leipzig"
  ]
  node [
    id 735
    label "Am Schmiedehoefchen, Taucha (Sachsen)"
  ]
  node [
    id 736
    label "Vergnuegungspark Belantis, Leipzig"
  ]
  node [
    id 737
    label "Apotheke, Hartmannsdorf b Chemnitz (Sachsen)"
  ]
  node [
    id 738
    label "Lindenthal Zur Lindenhoehe, Leipzig"
  ]
  node [
    id 739
    label "Zschampertaue, Leipzig"
  ]
  node [
    id 740
    label "Augustusplatz, Leipzig"
  ]
  node [
    id 741
    label "Menckestr., Leipzig"
  ]
  node [
    id 742
    label "Zwenkau Hafen"
  ]
  node [
    id 743
    label "Markkleeberg-West, Markkleeberg"
  ]
  node [
    id 744
    label "Leipziger Str., Freiberg (Sachsen)"
  ]
  node [
    id 745
    label "Muenzgasse, Leipzig"
  ]
  node [
    id 746
    label "Taucha (Sachsen) Bahnhof"
  ]
  node [
    id 747
    label "Leipziger Str/Schulweg, Freiberg (Sachsen)"
  ]
  node [
    id 748
    label "Stadthalle, Chemnitz"
  ]
  node [
    id 749
    label "Seehausener Allee/Messegelaende, Leipzig"
  ]
  node [
    id 750
    label "Holzhausen Bahnhof, Leipzig"
  ]
  node [
    id 751
    label "Fliederhof, Leipzig"
  ]
  node [
    id 752
    label "Rischmuehleninsel, Merseburg"
  ]
  node [
    id 753
    label "Glesien Koelsaer Str., Schkeuditz"
  ]
  node [
    id 754
    label "Gollmenz B2, Schoenwoelkau"
  ]
  node [
    id 755
    label "Rueckmarsdorf Sandberg, Leipzig"
  ]
  node [
    id 756
    label "Eula B95, Borna"
  ]
  node [
    id 757
    label "Ahornstr., Leipzig"
  ]
  node [
    id 758
    label "Baestleinstr./Schwantesstr., Leipzig"
  ]
  node [
    id 759
    label "Lindnerstr., Taucha (Sachsen)"
  ]
  node [
    id 760
    label "Cospudener See EXPO-Pavillon, Leipzig"
  ]
  node [
    id 761
    label "S-Bahnhof Gohlis, Leipzig"
  ]
  node [
    id 762
    label "Hopfenweg, Markranstaedt-Frankenheim"
  ]
  node [
    id 763
    label "Abzw. Schnauderhainichen, Meuselwitz"
  ]
  node [
    id 764
    label "S-Bahnhof Messe, Leipzig"
  ]
  node [
    id 765
    label "Volksgarten, Leipzig"
  ]
  node [
    id 766
    label "Fuchshain Schulstr., Naunhof"
  ]
  node [
    id 767
    label "Leipziger Str., Bennewitz"
  ]
  node [
    id 768
    label "Krensitz Bahnhof, Krostitz"
  ]
  node [
    id 769
    label "Rueckmarsdorf Zum Bahnhof, Leipzig"
  ]
  node [
    id 770
    label "Schoenauer Ring, Leipzig"
  ]
  node [
    id 771
    label "Schoenauer-Luetzner Str., Leipzig"
  ]
  node [
    id 772
    label "Grossdalzig Kirche, Zwenkau"
  ]
  node [
    id 773
    label "Konsum, Zwenkau"
  ]
  node [
    id 774
    label "Hamburger Str., Leipzig"
  ]
  node [
    id 775
    label "Nordplatz, Leipzig"
  ]
  node [
    id 776
    label "Connewitzer Str., Leipzig"
  ]
  node [
    id 777
    label "Ostplatz, Leipzig"
  ]
  node [
    id 778
    label "Moelkau Bahnhof, Leipzig"
  ]
  node [
    id 779
    label "Salzhandelsstr., Leipzig"
  ]
  node [
    id 780
    label "Bertolt-Brecht-Str., Leipzig"
  ]
  node [
    id 781
    label "An den Tierkliniken, Leipzig"
  ]
  node [
    id 782
    label "Karl-Heine-Str./Giesserstr., Leipzig"
  ]
  node [
    id 783
    label "Grossvoigtsberg Zellwald-Center, Grossschirma"
  ]
  node [
    id 784
    label "Schmiedeberger Str., Bad Dueben"
  ]
  node [
    id 785
    label "Engelsdorf Aerztehaus, Leipzig"
  ]
  node [
    id 786
    label "Wachau An der Hohle, Markkleeberg"
  ]
  node [
    id 787
    label "Stoetteritz Holzhaeuser Str., Leipzig"
  ]
  node [
    id 788
    label "Stahmeln, Leipzig"
  ]
  node [
    id 789
    label "Lindenthaler Hauptstr., Leipzig"
  ]
  node [
    id 790
    label "Lutherstr., Bad Dueben"
  ]
  node [
    id 791
    label "Delitzscher Str./Theresienstr., Leipzig"
  ]
  node [
    id 792
    label "Kuehnhaide, Hartmannsdorf b Chemnitz (Sachsen)"
  ]
  node [
    id 793
    label "Schleussig Karlbruecke, Leipzig"
  ]
  node [
    id 794
    label "Zwenkau Feuerwehr"
  ]
  node [
    id 795
    label "Olbrichtstr., Leipzig"
  ]
  node [
    id 796
    label "Fuchshainer Str., Grosspoesna"
  ]
  node [
    id 797
    label "Mariannenpark, Leipzig"
  ]
  node [
    id 798
    label "Gartenstadt, Schkeuditz"
  ]
  node [
    id 799
    label "Huttenstr., Leipzig"
  ]
  node [
    id 800
    label "Doelitz Strassenbahnhof, Leipzig"
  ]
  node [
    id 801
    label "Manteuffelstr., Taucha (Sachsen)"
  ]
  node [
    id 802
    label "Michael-Kazmierczak-Str., Leipzig"
  ]
  node [
    id 803
    label "Schongauerstr., Leipzig"
  ]
  node [
    id 804
    label "Dankwartstr., Leipzig"
  ]
  node [
    id 805
    label "Sonnesiedlung, Markkleeberg"
  ]
  node [
    id 806
    label "Stoeckelstr., Leipzig"
  ]
  node [
    id 807
    label "Delitzscher Str./Essener Str., Leipzig"
  ]
  node [
    id 808
    label "Kanalstr./Bachstr., Altenburg"
  ]
  node [
    id 809
    label "Ring, Markkleeberg"
  ]
  node [
    id 810
    label "Raschwitzer Str., Leipzig"
  ]
  node [
    id 811
    label "Karl-Heine-Str./Merseburger Str., Leipzig"
  ]
  node [
    id 812
    label "Sportforum, Leipzig"
  ]
  node [
    id 813
    label "Horburger Str., Leipzig"
  ]
  node [
    id 814
    label "Matthias-Erzberger-Str., Taucha (Sachsen)"
  ]
  node [
    id 815
    label "Miltitz (Strassenbahn), Leipzig"
  ]
  node [
    id 816
    label "Deuben Leipziger Str., Bennewitz"
  ]
  node [
    id 817
    label "Loebschuetz, Zwenkau"
  ]
  node [
    id 818
    label "Louis-Otto-Str., Chemnitz"
  ]
  node [
    id 819
    label "S-Bahnhof MDR, Leipzig"
  ]
  node [
    id 820
    label "Hauptstr., Kriebitzsch"
  ]
  node [
    id 821
    label "Gueldengossa, Grosspoesna"
  ]
  node [
    id 822
    label "Oertgering, Leipzig-Althen"
  ]
  node [
    id 823
    label "Wiebelstr., Leipzig"
  ]
  node [
    id 824
    label "Buenauroda, Meuselwitz"
  ]
  node [
    id 825
    label "Lindenau Str./Zeitzer Str., Altenburg"
  ]
  node [
    id 826
    label "Mockau Kirche, Leipzig"
  ]
  node [
    id 827
    label "Altes Messegelaende, Leipzig"
  ]
  node [
    id 828
    label "Ortsausgang Seegeritz, Taucha (Sachsen)"
  ]
  node [
    id 829
    label "Wodanstr., Leipzig"
  ]
  node [
    id 830
    label "Hospitalstr., Groitzsch b Pegau"
  ]
  node [
    id 831
    label "Probstheida Wendestelle, Leipzig"
  ]
  node [
    id 832
    label "Ziegelstrasse, Eilenburg"
  ]
  node [
    id 833
    label "Merseburger/Schomburgkstr., Leipzig"
  ]
  node [
    id 834
    label "Gundorf Friedhof, Leipzig"
  ]
  node [
    id 835
    label "Rosmarinweg, Leipzig"
  ]
  node [
    id 836
    label "Muehlau Sportplatz"
  ]
  node [
    id 837
    label "Zittauer Str., Leipzig"
  ]
  node [
    id 838
    label "Grethen Grimmaer Str., Parthenstein"
  ]
  node [
    id 839
    label "Naunhofer Str., Naunhof-Eicha"
  ]
  node [
    id 840
    label "Michaelisstr., Leipzig"
  ]
  node [
    id 841
    label "Zwenkau Nord, Zwenkau"
  ]
  node [
    id 842
    label "S-Bahnhof Allee-Center, Leipzig"
  ]
  node [
    id 843
    label "Hauptbahnhof Ostseite, Leipzig"
  ]
  node [
    id 844
    label "Obertitz, Groitzsch"
  ]
  node [
    id 845
    label "Naumburger Str., Leipzig"
  ]
  node [
    id 846
    label "Wittestr., Leipzig"
  ]
  node [
    id 847
    label "Lausener Str., Markranstaedt"
  ]
  node [
    id 848
    label "Hartzstr., Leipzig"
  ]
  node [
    id 849
    label "Delitzsch Buergerhaus"
  ]
  node [
    id 850
    label "Mockau Post, Leipzig"
  ]
  node [
    id 851
    label "Leipziger Str./Schule, Rackwitz"
  ]
  node [
    id 852
    label "Dreiskau-Muckern Dreiskau, Grosspoesna"
  ]
  node [
    id 853
    label "Prager/Russenstr., Leipzig"
  ]
  node [
    id 854
    label "Moelkau Hasenheide, Leipzig"
  ]
  node [
    id 855
    label "Fritz-Seger-Str., Leipzig"
  ]
  node [
    id 856
    label "Lindenauer Markt, Leipzig"
  ]
  node [
    id 857
    label "Werk, Rositz"
  ]
  node [
    id 858
    label "Saturnstr., Leipzig"
  ]
  node [
    id 859
    label "Lidicestr., Leipzig"
  ]
  node [
    id 860
    label "Altenburger Str., Groitzsch b Pegau"
  ]
  node [
    id 861
    label "Landsberger Str., Wiedemar-Koelsa"
  ]
  node [
    id 862
    label "Dornbergerstr., Leipzig"
  ]
  node [
    id 863
    label "Lausen Aerztehaus, Leipzig"
  ]
  node [
    id 864
    label "Sosaer Str., Leipzig"
  ]
  node [
    id 865
    label "Weissestr., Leipzig"
  ]
  node [
    id 866
    label "Rueckmarsdorf Zur Kaninchenfarm, Leipzig"
  ]
  node [
    id 867
    label "Eutritzsch Markt, Leipzig"
  ]
  node [
    id 868
    label "Seehausen Schule, Leipzig"
  ]
  node [
    id 869
    label "Wielandstr., Leipzig"
  ]
  node [
    id 870
    label "GVZ Sued/Am Exer, Leipzig"
  ]
  node [
    id 871
    label "Friedensdorf Abzw., Luppenau"
  ]
  node [
    id 872
    label "Lindenaundorf Schmiede, Markranstaedt"
  ]
  node [
    id 873
    label "Messegelaende, Leipzig"
  ]
  node [
    id 874
    label "Bismarckstr., Leipzig"
  ]
  node [
    id 875
    label "Julian-Marchlewski-Str., Leipzig"
  ]
  node [
    id 876
    label "Forsthaus Raschwitz, Markkleeberg"
  ]
  node [
    id 877
    label "Rackwitz Busbahnhof"
  ]
  node [
    id 878
    label "Bogumils Garten, Taucha (Sachsen)"
  ]
  node [
    id 879
    label "Brandiser Str., Naunhof"
  ]
  node [
    id 880
    label "Abzw. Siedlerstr., Naunhof"
  ]
  node [
    id 881
    label "Braunstr./Bautzner Str., Leipzig"
  ]
  node [
    id 882
    label "Wilhelm-Pfennig-Str., Leipzig"
  ]
  node [
    id 883
    label "Brodau B184, Doebernitz"
  ]
  node [
    id 884
    label "Karl-Jungbluth-Str./S-Bahnhof Connewitz, Leipzig"
  ]
  node [
    id 885
    label "Am Langen Teiche, Leipzig"
  ]
  node [
    id 886
    label "Proettitz, Krostitz"
  ]
  node [
    id 887
    label "Stoermthal Aussenstelle Gemeindeverwaltung, Grosspoesn"
  ]
  node [
    id 888
    label "Nova Eventis, Guenthersdorf b Merseburg"
  ]
  node [
    id 889
    label "Thomaskirche, Leipzig"
  ]
  node [
    id 890
    label "Wildpark, Leipzig"
  ]
  node [
    id 891
    label "Baumeisterallee, Zwenkau"
  ]
  node [
    id 892
    label "Geisslerstr. Buelowviertel, Leipzig"
  ]
  node [
    id 893
    label "Dewitz Wachbergsiedlung, Taucha (Sachsen)"
  ]
  node [
    id 894
    label "Teschstr., Leipzig"
  ]
  node [
    id 895
    label "Georg-Herwegh-Str., Leipzig"
  ]
  node [
    id 896
    label "S-Bahnhof Stoetteritz, Leipzig"
  ]
  node [
    id 897
    label "Luetznerstr./Merseburger Str., Leipzig"
  ]
  node [
    id 898
    label "Meusdorfer Str., Leipzig"
  ]
  node [
    id 899
    label "Staedtelner Str., Markkleeberg"
  ]
  node [
    id 900
    label "Klinikum St. Georg, Leipzig"
  ]
  node [
    id 901
    label "Lise-Meitner-Str., Leipzig"
  ]
  node [
    id 902
    label "Georg-Schumann-/Luetzowstr., Leipzig"
  ]
  node [
    id 903
    label "Berndorf Gemeindeverwaltung, Groitzsch b Pegau"
  ]
  node [
    id 904
    label "Tellschuetz, Zwenkau"
  ]
  node [
    id 905
    label "Kotzschbarhoehe, Zwenkau"
  ]
  node [
    id 906
    label "Elster-Passage, Leipzig"
  ]
  node [
    id 907
    label "Slevogtstr., Leipzig"
  ]
  node [
    id 908
    label "Zweenfurther Str., Leipzig"
  ]
  node [
    id 909
    label "Moritz-Hof, Leipzig"
  ]
  node [
    id 910
    label "Probstheidaer-/Willi-Bredel-Str., Leipzig"
  ]
  node [
    id 911
    label "Hartmannsdorf Erikenstr., Leipzig"
  ]
  node [
    id 912
    label "Heiterblickstr., Leipzig"
  ]
  node [
    id 913
    label "Klein Schkorlopp, Kitzen"
  ]
  node [
    id 914
    label "Uhlandweg, Leipzig"
  ]
  node [
    id 915
    label "Eilenburg Bahnhof"
  ]
  node [
    id 916
    label "Feldstr., Grosspoesna"
  ]
  node [
    id 917
    label "Priesteblich, Markranstaedt"
  ]
  node [
    id 918
    label "Edlichstr., Leipzig"
  ]
  node [
    id 919
    label "Riebeckstr./Stoetteritzer Str., Leipzig"
  ]
  node [
    id 920
    label "Geschwister-Scholl- Str., Taucha (Sachsen)"
  ]
  node [
    id 921
    label "Bergstr./Ecke Muehlstr., Eilenburg"
  ]
  node [
    id 922
    label "Portitz Kraetzbergstr., Leipzig"
  ]
  node [
    id 923
    label "Lindenthal Buchfinkenweg, Leipzig"
  ]
  node [
    id 924
    label "Hohe Bruecke, Chemnitz"
  ]
  node [
    id 925
    label "Goethestrasse, Leipzig"
  ]
  node [
    id 926
    label "Voelkerschlachtdenkmal, Leipzig"
  ]
  node [
    id 927
    label "Leutzsch Strassenbahnhof, Leipzig"
  ]
  node [
    id 928
    label "Harthsiedlung, Zwenkau"
  ]
  node [
    id 929
    label "Glesien Ernst-Thaelmann-Str., Schkeuditz"
  ]
  node [
    id 930
    label "Poesna-Park, Grosspoesna"
  ]
  node [
    id 931
    label "Alte Salzstr., Leipzig"
  ]
  node [
    id 932
    label "Gottschallstr., Leipzig"
  ]
  node [
    id 933
    label "Hornbach Baumarkt, Leipzig"
  ]
  node [
    id 934
    label "Lortzingstr., Leipzig"
  ]
  node [
    id 935
    label "Engelsdorf Werkstaettenstr., Leipzig"
  ]
  node [
    id 936
    label "Holsteinstr., Leipzig"
  ]
  node [
    id 937
    label "Leipzig Hauptbahnhof"
  ]
  node [
    id 938
    label "Angerbruecke, Leipzig"
  ]
  node [
    id 939
    label "Wolfgang-Heinze-/Meusdorfer Str., Leipzig"
  ]
  node [
    id 940
    label "Allee-Center (Sued), Leipzig"
  ]
  node [
    id 941
    label "Hans-Weigel-Str., Leipzig"
  ]
  node [
    id 942
    label "Holbeinstr., Leipzig"
  ]
  node [
    id 943
    label "Leipziger Str., Taucha (Sachsen)"
  ]
  node [
    id 944
    label "Johannisplatz, Leipzig"
  ]
  node [
    id 945
    label "Doelzig Hollaendische Muehle, Schkeuditz"
  ]
  node [
    id 946
    label "Klingerweg, Leipzig"
  ]
  node [
    id 947
    label "S-Bahnhof Nord (Bus), Markkleeberg"
  ]
  node [
    id 948
    label "Liebertwolkwitz Stoermthaler Str., Leipzig"
  ]
  node [
    id 949
    label "Seepromenade, Markkleeberg"
  ]
  node [
    id 950
    label "Kiewer Str., Leipzig"
  ]
  node [
    id 951
    label "Goehrenz Schachthaeuser, Markranstaedt"
  ]
  node [
    id 952
    label "Cred&#233;str., Leipzig"
  ]
  node [
    id 953
    label "Breite Str., Leipzig"
  ]
  node [
    id 954
    label "Falkenstr., Leipzig"
  ]
  node [
    id 955
    label "Wartehalle, Jesewitz"
  ]
  node [
    id 956
    label "Am Teich, Kitzen"
  ]
  edge [
    source 0
    target 87
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 0
    target 726
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 1
    target 282
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 1
    target 733
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 1
    target 805
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 1
    target 261
    line_type "bus"
    line_number "108"
  ]
  edge [
    source 1
    target 600
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 2
    target 534
    line_type "str"
    line_number "7"
  ]
  edge [
    source 2
    target 617
    line_type "str"
    line_number "7"
  ]
  edge [
    source 3
    target 300
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 3
    target 65
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 4
    target 921
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 4
    target 473
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 5
    target 355
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 5
    target 233
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 6
    target 442
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 6
    target 120
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 7
    target 389
    line_type "bus"
    line_number "85"
  ]
  edge [
    source 8
    target 46
    line_type "bus"
    line_number "175"
  ]
  edge [
    source 8
    target 893
    line_type "bus"
    line_number "175"
  ]
  edge [
    source 9
    target 757
    line_type "bus"
    line_number "n7"
  ]
  edge [
    source 9
    target 85
    line_type "str"
    line_number "7"
  ]
  edge [
    source 9
    target 549
    line_type "bus"
    line_number "n7"
  ]
  edge [
    source 10
    target 625
    line_type "sev"
    line_number "8"
  ]
  edge [
    source 10
    target 81
    line_type "sev"
    line_number "8"
  ]
  edge [
    source 10
    target 648
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 10
    target 862
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 11
    target 682
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 11
    target 478
    line_type "bus"
    line_number "86"
  ]
  edge [
    source 11
    target 658
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 12
    target 152
    line_type "bus"
    line_number "60"
  ]
  edge [
    source 12
    target 286
    line_type "bus"
    line_number "60"
  ]
  edge [
    source 13
    target 467
    line_type "str"
    line_number "9"
  ]
  edge [
    source 13
    target 324
    line_type "str"
    line_number "9"
  ]
  edge [
    source 14
    target 556
    line_type "bus"
    line_number "61"
  ]
  edge [
    source 15
    target 385
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 15
    target 454
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 16
    target 519
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 16
    target 108
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 17
    target 311
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 17
    target 807
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 17
    target 715
    line_type "bus"
    line_number "85"
  ]
  edge [
    source 18
    target 837
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 18
    target 535
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 19
    target 408
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 19
    target 921
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 20
    target 689
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 20
    target 847
    line_type "bus"
    line_number "161"
  ]
  edge [
    source 20
    target 547
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 21
    target 478
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 21
    target 397
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 21
    target 341
    line_type "bus"
    line_number "175"
  ]
  edge [
    source 22
    target 245
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 23
    target 237
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 23
    target 749
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 23
    target 677
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 23
    target 178
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 24
    target 68
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 24
    target 786
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 24
    target 30
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 25
    target 873
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 25
    target 431
    line_type "bus"
    line_number "86"
  ]
  edge [
    source 25
    target 677
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 26
    target 445
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 26
    target 558
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 27
    target 948
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 27
    target 337
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 28
    target 128
    line_type "str"
    line_number "32"
  ]
  edge [
    source 28
    target 350
    line_type "str"
    line_number "32"
  ]
  edge [
    source 29
    target 773
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 30
    target 595
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 31
    target 685
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 31
    target 616
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 32
    target 693
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 32
    target 335
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 33
    target 145
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 33
    target 136
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 34
    target 279
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 34
    target 712
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 34
    target 188
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 34
    target 516
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 35
    target 352
    line_type "bus"
    line_number "61"
  ]
  edge [
    source 35
    target 557
    line_type "bus"
    line_number "61"
  ]
  edge [
    source 36
    target 881
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 36
    target 451
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 37
    target 628
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 37
    target 103
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 37
    target 817
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 38
    target 311
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 38
    target 850
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 39
    target 167
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 39
    target 161
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 39
    target 786
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 40
    target 426
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 40
    target 353
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 41
    target 768
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 41
    target 754
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 42
    target 698
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 42
    target 309
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 43
    target 637
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 43
    target 727
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 44
    target 703
    line_type "bus"
    line_number "108"
  ]
  edge [
    source 44
    target 490
    line_type "bus"
    line_number "108"
  ]
  edge [
    source 45
    target 364
    line_type "bus"
    line_number "n17"
  ]
  edge [
    source 45
    target 291
    line_type "bus"
    line_number "n17"
  ]
  edge [
    source 45
    target 687
    line_type "str"
    line_number "3"
  ]
  edge [
    source 45
    target 334
    line_type "str"
    line_number "3"
  ]
  edge [
    source 46
    target 920
    line_type "bus"
    line_number "175"
  ]
  edge [
    source 47
    target 529
    line_type "bus"
    line_number "n3"
  ]
  edge [
    source 47
    target 506
    line_type "bus"
    line_number "n3"
  ]
  edge [
    source 47
    target 952
    line_type "str"
    line_number "15"
  ]
  edge [
    source 47
    target 310
    line_type "str"
    line_number "15"
  ]
  edge [
    source 48
    target 941
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 48
    target 304
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 49
    target 93
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 49
    target 122
    line_type "str"
    line_number "9"
  ]
  edge [
    source 49
    target 740
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 49
    target 843
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 49
    target 147
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 50
    target 105
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 50
    target 442
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 51
    target 165
    line_type "str"
    line_number "39"
  ]
  edge [
    source 51
    target 732
    line_type "str"
    line_number "39"
  ]
  edge [
    source 52
    target 60
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 52
    target 930
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 52
    target 691
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 53
    target 311
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 54
    target 83
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 54
    target 943
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 55
    target 890
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 55
    target 939
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 55
    target 460
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 55
    target 472
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 55
    target 794
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 56
    target 613
    line_type "sev"
    line_number "11"
  ]
  edge [
    source 56
    target 810
    line_type "sev"
    line_number "11"
  ]
  edge [
    source 57
    target 900
    line_type "sev"
    line_number "16"
  ]
  edge [
    source 57
    target 96
    line_type "sev"
    line_number "16"
  ]
  edge [
    source 57
    target 254
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 57
    target 189
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 57
    target 346
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 58
    target 564
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 58
    target 322
    line_type "bus"
    line_number "72"
  ]
  edge [
    source 58
    target 822
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 58
    target 412
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 58
    target 404
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 59
    target 120
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 59
    target 790
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 60
    target 661
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 61
    target 882
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 61
    target 416
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 62
    target 695
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 62
    target 929
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 63
    target 749
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 63
    target 346
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 64
    target 828
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 64
    target 508
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 65
    target 163
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 65
    target 414
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 65
    target 867
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 66
    target 215
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 67
    target 701
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 67
    target 219
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 68
    target 470
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 68
    target 786
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 69
    target 605
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 69
    target 253
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 70
    target 770
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 71
    target 196
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 71
    target 645
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 72
    target 673
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 72
    target 196
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 73
    target 260
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 73
    target 183
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 74
    target 495
    line_type "bus"
    line_number "n1"
  ]
  edge [
    source 74
    target 342
    line_type "bus"
    line_number "n1"
  ]
  edge [
    source 74
    target 630
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 74
    target 696
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 75
    target 296
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 75
    target 710
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 76
    target 511
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 76
    target 186
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 77
    target 517
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 78
    target 480
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 78
    target 583
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 79
    target 459
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 79
    target 792
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 80
    target 328
    line_type "bus"
    line_number "90"
  ]
  edge [
    source 80
    target 549
    line_type "bus"
    line_number "90"
  ]
  edge [
    source 81
    target 373
    line_type "sev"
    line_number "8"
  ]
  edge [
    source 82
    target 374
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 82
    target 908
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 83
    target 943
    line_type "bus"
    line_number "176"
  ]
  edge [
    source 83
    target 920
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 84
    target 710
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 84
    target 852
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 85
    target 546
    line_type "str"
    line_number "7"
  ]
  edge [
    source 85
    target 99
    line_type "bus"
    line_number "72"
  ]
  edge [
    source 86
    target 752
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 86
    target 94
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 87
    target 524
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 88
    target 411
    line_type "str"
    line_number "3"
  ]
  edge [
    source 88
    target 317
    line_type "str"
    line_number "3"
  ]
  edge [
    source 89
    target 575
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 89
    target 308
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 90
    target 332
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 90
    target 207
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 91
    target 485
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 91
    target 563
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 91
    target 114
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 91
    target 647
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 92
    target 415
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 92
    target 156
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 93
    target 147
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 93
    target 728
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 95
    target 164
    line_type "str"
    line_number "11"
  ]
  edge [
    source 95
    target 392
    line_type "str"
    line_number "11"
  ]
  edge [
    source 96
    target 895
    line_type "sev"
    line_number "16"
  ]
  edge [
    source 97
    target 549
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 97
    target 659
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 98
    target 288
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 98
    target 466
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 99
    target 803
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 99
    target 549
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 100
    target 476
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 101
    target 481
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 101
    target 641
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 102
    target 183
    line_type "bus"
    line_number "175"
  ]
  edge [
    source 102
    target 510
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 102
    target 509
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 103
    target 772
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 104
    target 181
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 104
    target 250
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 105
    target 754
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 106
    target 708
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 106
    target 611
    line_type "str"
    line_number "9"
  ]
  edge [
    source 106
    target 354
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 107
    target 614
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 107
    target 155
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 108
    target 259
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 109
    target 840
    line_type "str"
    line_number "7"
  ]
  edge [
    source 109
    target 937
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 109
    target 264
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 109
    target 405
    line_type "str"
    line_number "9"
  ]
  edge [
    source 109
    target 559
    line_type "str"
    line_number "10"
  ]
  edge [
    source 109
    target 636
    line_type "str"
    line_number "9"
  ]
  edge [
    source 110
    target 652
    line_type "str"
    line_number "3"
  ]
  edge [
    source 110
    target 247
    line_type "str"
    line_number "3"
  ]
  edge [
    source 111
    target 646
    line_type "sev"
    line_number "11"
  ]
  edge [
    source 111
    target 358
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 111
    target 949
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 111
    target 312
    line_type "bus"
    line_number "108"
  ]
  edge [
    source 112
    target 702
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 112
    target 400
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 113
    target 416
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 113
    target 409
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 114
    target 647
    line_type "bus"
    line_number "87"
  ]
  edge [
    source 114
    target 657
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 115
    target 578
    line_type "bus"
    line_number "162"
  ]
  edge [
    source 115
    target 242
    line_type "bus"
    line_number "162"
  ]
  edge [
    source 115
    target 191
    line_type "bus"
    line_number "162"
  ]
  edge [
    source 116
    target 640
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 116
    target 175
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 117
    target 850
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 117
    target 706
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 118
    target 317
    line_type "str"
    line_number "3"
  ]
  edge [
    source 118
    target 696
    line_type "str"
    line_number "3"
  ]
  edge [
    source 119
    target 182
    line_type "str"
    line_number "39"
  ]
  edge [
    source 119
    target 153
    line_type "str"
    line_number "39"
  ]
  edge [
    source 121
    target 501
    line_type "bus"
    line_number "175"
  ]
  edge [
    source 121
    target 260
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 121
    target 140
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 122
    target 740
    line_type "sev"
    line_number "10"
  ]
  edge [
    source 122
    target 543
    line_type "str"
    line_number "9"
  ]
  edge [
    source 122
    target 745
    line_type "bus"
    line_number "n9"
  ]
  edge [
    source 122
    target 622
    line_type "sev"
    line_number "10"
  ]
  edge [
    source 123
    target 269
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 123
    target 888
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 124
    target 269
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 124
    target 210
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 125
    target 248
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 125
    target 553
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 126
    target 433
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 126
    target 691
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 127
    target 716
    line_type "str"
    line_number "12"
  ]
  edge [
    source 127
    target 777
    line_type "str"
    line_number "12"
  ]
  edge [
    source 128
    target 946
    line_type "bus"
    line_number "n17"
  ]
  edge [
    source 128
    target 530
    line_type "bus"
    line_number "n17"
  ]
  edge [
    source 129
    target 707
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 130
    target 732
    line_type "str"
    line_number "39"
  ]
  edge [
    source 130
    target 741
    line_type "str"
    line_number "39"
  ]
  edge [
    source 131
    target 310
    line_type "str"
    line_number "15"
  ]
  edge [
    source 131
    target 770
    line_type "str"
    line_number "15"
  ]
  edge [
    source 132
    target 384
    line_type "bus"
    line_number "162"
  ]
  edge [
    source 132
    target 799
    line_type "bus"
    line_number "162"
  ]
  edge [
    source 132
    target 874
    line_type "bus"
    line_number "162"
  ]
  edge [
    source 133
    target 143
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 133
    target 204
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 134
    target 804
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 134
    target 810
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 135
    target 766
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 135
    target 420
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 135
    target 347
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 136
    target 285
    line_type "bus"
    line_number "n7"
  ]
  edge [
    source 136
    target 482
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 137
    target 382
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 137
    target 343
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 138
    target 446
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 138
    target 737
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 139
    target 758
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 139
    target 235
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 140
    target 552
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 141
    target 489
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 141
    target 434
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 142
    target 813
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 142
    target 326
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 143
    target 590
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 144
    target 587
    line_type "str"
    line_number "7"
  ]
  edge [
    source 144
    target 693
    line_type "str"
    line_number "7"
  ]
  edge [
    source 145
    target 778
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 145
    target 714
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 146
    target 330
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 146
    target 532
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 147
    target 561
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 147
    target 208
    line_type "str"
    line_number "9"
  ]
  edge [
    source 147
    target 150
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 148
    target 668
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 148
    target 555
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 149
    target 274
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 149
    target 433
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 150
    target 208
    line_type "str"
    line_number "9"
  ]
  edge [
    source 150
    target 511
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 150
    target 585
    line_type "bus"
    line_number "n9"
  ]
  edge [
    source 150
    target 513
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 150
    target 472
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 151
    target 827
    line_type "str"
    line_number "2"
  ]
  edge [
    source 151
    target 561
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 151
    target 819
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 151
    target 716
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 151
    target 781
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 152
    target 484
    line_type "bus"
    line_number "60"
  ]
  edge [
    source 153
    target 734
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 153
    target 400
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 153
    target 795
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 154
    target 534
    line_type "bus"
    line_number "67"
  ]
  edge [
    source 155
    target 694
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 156
    target 724
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 157
    target 607
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 157
    target 585
    line_type "bus"
    line_number "n9"
  ]
  edge [
    source 157
    target 440
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 158
    target 333
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 158
    target 241
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 159
    target 575
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 159
    target 399
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 160
    target 799
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 160
    target 411
    line_type "str"
    line_number "3"
  ]
  edge [
    source 160
    target 874
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 161
    target 435
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 162
    target 730
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 162
    target 266
    line_type "bus"
    line_number "n17"
  ]
  edge [
    source 162
    target 931
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 163
    target 848
    line_type "bus"
    line_number "90"
  ]
  edge [
    source 163
    target 791
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 164
    target 798
    line_type "str"
    line_number "11"
  ]
  edge [
    source 165
    target 255
    line_type "str"
    line_number "39"
  ]
  edge [
    source 165
    target 812
    line_type "str"
    line_number "7"
  ]
  edge [
    source 165
    target 530
    line_type "str"
    line_number "8"
  ]
  edge [
    source 166
    target 477
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 166
    target 612
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 166
    target 659
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 166
    target 496
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 167
    target 712
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 167
    target 337
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 168
    target 868
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 168
    target 688
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 169
    target 762
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 169
    target 251
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 170
    target 244
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 170
    target 355
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 171
    target 568
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 171
    target 509
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 172
    target 784
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 172
    target 528
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 173
    target 318
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 173
    target 408
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 174
    target 486
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 174
    target 298
    line_type "bus"
    line_number "n3"
  ]
  edge [
    source 174
    target 950
    line_type "bus"
    line_number "n2"
  ]
  edge [
    source 174
    target 277
    line_type "bus"
    line_number "n2"
  ]
  edge [
    source 174
    target 225
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 175
    target 418
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 176
    target 854
    line_type "bus"
    line_number "72"
  ]
  edge [
    source 176
    target 305
    line_type "bus"
    line_number "72"
  ]
  edge [
    source 177
    target 777
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 177
    target 561
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 178
    target 301
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 179
    target 313
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 179
    target 676
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 180
    target 412
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 180
    target 232
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 181
    target 238
    line_type "bus"
    line_number "83"
  ]
  edge [
    source 181
    target 223
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 181
    target 449
    line_type "bus"
    line_number "86"
  ]
  edge [
    source 182
    target 413
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 184
    target 494
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 184
    target 278
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 185
    target 839
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 185
    target 457
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 186
    target 554
    line_type "bus"
    line_number "60"
  ]
  edge [
    source 186
    target 602
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 187
    target 545
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 187
    target 307
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 188
    target 314
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 188
    target 516
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 189
    target 603
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 190
    target 721
    line_type "bus"
    line_number "n1"
  ]
  edge [
    source 190
    target 630
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 190
    target 427
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 191
    target 863
    line_type "bus"
    line_number "61"
  ]
  edge [
    source 191
    target 723
    line_type "bus"
    line_number "162"
  ]
  edge [
    source 192
    target 246
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 193
    target 581
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 194
    target 678
    line_type "bus"
    line_number "161"
  ]
  edge [
    source 194
    target 268
    line_type "bus"
    line_number "161"
  ]
  edge [
    source 195
    target 354
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 195
    target 850
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 197
    target 510
    line_type "bus"
    line_number "176"
  ]
  edge [
    source 197
    target 650
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 197
    target 259
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 198
    target 488
    line_type "bus"
    line_number "n17"
  ]
  edge [
    source 198
    target 520
    line_type "bus"
    line_number "n17"
  ]
  edge [
    source 199
    target 936
    line_type "bus"
    line_number "60"
  ]
  edge [
    source 200
    target 493
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 200
    target 833
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 201
    target 886
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 201
    target 393
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 202
    target 724
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 202
    target 870
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 203
    target 537
    line_type "str"
    line_number "11"
  ]
  edge [
    source 203
    target 323
    line_type "str"
    line_number "11"
  ]
  edge [
    source 204
    target 925
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 205
    target 826
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 205
    target 338
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 205
    target 706
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 205
    target 859
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 206
    target 268
    line_type "bus"
    line_number "161"
  ]
  edge [
    source 206
    target 863
    line_type "bus"
    line_number "61"
  ]
  edge [
    source 206
    target 634
    line_type "bus"
    line_number "161"
  ]
  edge [
    source 207
    target 857
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 209
    target 916
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 209
    target 662
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 210
    target 945
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 211
    target 792
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 211
    target 660
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 212
    target 246
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 212
    target 639
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 213
    target 321
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 213
    target 589
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 214
    target 335
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 214
    target 834
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 215
    target 299
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 216
    target 221
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 216
    target 515
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 217
    target 722
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 217
    target 670
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 218
    target 422
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 218
    target 756
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 219
    target 453
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 220
    target 912
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 220
    target 375
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 220
    target 859
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 221
    target 849
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 222
    target 369
    line_type "bus"
    line_number "e"
  ]
  edge [
    source 222
    target 878
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 222
    target 801
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 223
    target 426
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 224
    target 372
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 224
    target 572
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 225
    target 298
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 226
    target 464
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 226
    target 690
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 226
    target 412
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 227
    target 856
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 228
    target 635
    line_type "str"
    line_number "39"
  ]
  edge [
    source 228
    target 591
    line_type "str"
    line_number "39"
  ]
  edge [
    source 229
    target 777
    line_type "str"
    line_number "12"
  ]
  edge [
    source 229
    target 944
    line_type "str"
    line_number "12"
  ]
  edge [
    source 230
    target 477
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 230
    target 881
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 230
    target 780
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 230
    target 758
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 231
    target 302
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 231
    target 313
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 232
    target 778
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 233
    target 830
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 234
    target 252
    line_type "str"
    line_number "11"
  ]
  edge [
    source 234
    target 798
    line_type "str"
    line_number "11"
  ]
  edge [
    source 235
    target 535
    line_type "bus"
    line_number "90"
  ]
  edge [
    source 235
    target 666
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 236
    target 243
    line_type "bus"
    line_number "175"
  ]
  edge [
    source 236
    target 297
    line_type "bus"
    line_number "175"
  ]
  edge [
    source 237
    target 401
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 238
    target 699
    line_type "bus"
    line_number "83"
  ]
  edge [
    source 239
    target 472
    line_type "sev"
    line_number "10"
  ]
  edge [
    source 239
    target 898
    line_type "sev"
    line_number "10"
  ]
  edge [
    source 240
    target 666
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 240
    target 328
    line_type "bus"
    line_number "90"
  ]
  edge [
    source 240
    target 531
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 241
    target 654
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 243
    target 893
    line_type "bus"
    line_number "175"
  ]
  edge [
    source 244
    target 588
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 245
    target 410
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 247
    target 799
    line_type "str"
    line_number "3"
  ]
  edge [
    source 248
    target 783
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 249
    target 312
    line_type "bus"
    line_number "105"
  ]
  edge [
    source 249
    target 490
    line_type "bus"
    line_number "105"
  ]
  edge [
    source 249
    target 947
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 250
    target 922
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 251
    target 269
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 252
    target 323
    line_type "str"
    line_number "11"
  ]
  edge [
    source 253
    target 448
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 254
    target 371
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 255
    target 258
    line_type "str"
    line_number "4"
  ]
  edge [
    source 255
    target 934
    line_type "str"
    line_number "7"
  ]
  edge [
    source 255
    target 889
    line_type "str"
    line_number "39"
  ]
  edge [
    source 256
    target 799
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 256
    target 760
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 257
    target 849
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 258
    target 937
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 258
    target 395
    line_type "bus"
    line_number "n2"
  ]
  edge [
    source 258
    target 934
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 258
    target 889
    line_type "str"
    line_number "9"
  ]
  edge [
    source 258
    target 405
    line_type "str"
    line_number "9"
  ]
  edge [
    source 259
    target 717
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 259
    target 580
    line_type "bus"
    line_number "n7"
  ]
  edge [
    source 259
    target 954
    line_type "bus"
    line_number "e"
  ]
  edge [
    source 259
    target 650
    line_type "bus"
    line_number "176"
  ]
  edge [
    source 259
    target 925
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 261
    target 899
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 261
    target 551
    line_type "bus"
    line_number "108"
  ]
  edge [
    source 262
    target 515
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 262
    target 480
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 263
    target 883
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 263
    target 372
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 264
    target 902
    line_type "str"
    line_number "9"
  ]
  edge [
    source 264
    target 775
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 264
    target 414
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 264
    target 791
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 265
    target 419
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 265
    target 692
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 266
    target 640
    line_type "bus"
    line_number "n17"
  ]
  edge [
    source 267
    target 631
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 267
    target 727
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 269
    target 755
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 270
    target 770
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 270
    target 813
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 271
    target 808
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 271
    target 351
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 272
    target 576
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 272
    target 814
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 273
    target 747
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 273
    target 594
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 274
    target 471
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 275
    target 860
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 275
    target 667
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 276
    target 315
    line_type "bus"
    line_number "n9"
  ]
  edge [
    source 276
    target 745
    line_type "bus"
    line_number "n9"
  ]
  edge [
    source 277
    target 385
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 277
    target 494
    line_type "bus"
    line_number "n2"
  ]
  edge [
    source 277
    target 486
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 277
    target 298
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 278
    target 689
    line_type "bus"
    line_number "n2"
  ]
  edge [
    source 278
    target 542
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 279
    target 606
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 280
    target 440
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 280
    target 642
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 280
    target 565
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 280
    target 781
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 281
    target 484
    line_type "str"
    line_number "15"
  ]
  edge [
    source 281
    target 897
    line_type "str"
    line_number "15"
  ]
  edge [
    source 282
    target 362
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 283
    target 757
    line_type "bus"
    line_number "n7"
  ]
  edge [
    source 283
    target 580
    line_type "bus"
    line_number "e"
  ]
  edge [
    source 283
    target 369
    line_type "bus"
    line_number "e"
  ]
  edge [
    source 283
    target 746
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 283
    target 717
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 284
    target 608
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 284
    target 386
    line_type "bus"
    line_number "n4"
  ]
  edge [
    source 284
    target 719
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 285
    target 305
    line_type "bus"
    line_number "72"
  ]
  edge [
    source 285
    target 482
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 285
    target 638
    line_type "bus"
    line_number "n7"
  ]
  edge [
    source 285
    target 399
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 285
    target 406
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 286
    target 432
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 286
    target 538
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 287
    target 833
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 287
    target 432
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 288
    target 763
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 289
    target 429
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 289
    target 614
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 290
    target 515
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 290
    target 883
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 291
    target 720
    line_type "bus"
    line_number "60"
  ]
  edge [
    source 291
    target 520
    line_type "bus"
    line_number "n17"
  ]
  edge [
    source 292
    target 331
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 292
    target 518
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 293
    target 567
    line_type "bus"
    line_number "90"
  ]
  edge [
    source 293
    target 674
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 293
    target 795
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 294
    target 770
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 294
    target 950
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 294
    target 540
    line_type "bus"
    line_number "161"
  ]
  edge [
    source 295
    target 514
    line_type "bus"
    line_number "76"
  ]
  edge [
    source 295
    target 663
    line_type "bus"
    line_number "76"
  ]
  edge [
    source 296
    target 461
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 297
    target 430
    line_type "bus"
    line_number "175"
  ]
  edge [
    source 298
    target 592
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 298
    target 629
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 299
    target 541
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 300
    target 932
    line_type "str"
    line_number "12"
  ]
  edge [
    source 300
    target 761
    line_type "str"
    line_number "12"
  ]
  edge [
    source 300
    target 802
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 301
    target 886
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 302
    target 623
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 303
    target 680
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 303
    target 862
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 304
    target 404
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 306
    target 541
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 306
    target 822
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 307
    target 752
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 308
    target 423
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 309
    target 541
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 311
    target 850
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 312
    target 358
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 312
    target 736
    line_type "bus"
    line_number "105"
  ]
  edge [
    source 314
    target 532
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 314
    target 516
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 315
    target 511
    line_type "bus"
    line_number "n9"
  ]
  edge [
    source 316
    target 361
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 316
    target 707
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 318
    target 915
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 319
    target 380
    line_type "bus"
    line_number "n3"
  ]
  edge [
    source 319
    target 815
    line_type "bus"
    line_number "n3"
  ]
  edge [
    source 320
    target 403
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 320
    target 903
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 321
    target 794
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 322
    target 344
    line_type "bus"
    line_number "72"
  ]
  edge [
    source 324
    target 389
    line_type "str"
    line_number "9"
  ]
  edge [
    source 325
    target 861
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 325
    target 421
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 326
    target 436
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 327
    target 680
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 327
    target 373
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 329
    target 531
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 329
    target 835
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 330
    target 786
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 331
    target 670
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 332
    target 512
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 333
    target 463
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 334
    target 652
    line_type "str"
    line_number "3"
  ]
  edge [
    source 336
    target 832
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 336
    target 408
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 337
    target 665
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 337
    target 712
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 338
    target 864
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 338
    target 458
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 338
    target 649
    line_type "bus"
    line_number "83"
  ]
  edge [
    source 339
    target 814
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 339
    target 497
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 340
    target 887
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 340
    target 424
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 341
    target 449
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 341
    target 828
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 342
    target 911
    line_type "bus"
    line_number "n1"
  ]
  edge [
    source 343
    target 631
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 344
    target 935
    line_type "bus"
    line_number "72"
  ]
  edge [
    source 345
    target 466
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 345
    target 507
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 346
    target 566
    line_type "bus"
    line_number "85"
  ]
  edge [
    source 346
    target 396
    line_type "bus"
    line_number "85"
  ]
  edge [
    source 346
    target 843
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 346
    target 873
    line_type "sev"
    line_number "16"
  ]
  edge [
    source 346
    target 895
    line_type "sev"
    line_number "16"
  ]
  edge [
    source 347
    target 839
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 348
    target 690
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 348
    target 714
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 349
    target 360
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 350
    target 938
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 350
    target 811
    line_type "bus"
    line_number "n2"
  ]
  edge [
    source 350
    target 906
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 352
    target 653
    line_type "bus"
    line_number "61"
  ]
  edge [
    source 353
    target 376
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 356
    target 574
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 356
    target 533
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 357
    target 743
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 357
    target 899
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 358
    target 743
    line_type "bus"
    line_number "n9"
  ]
  edge [
    source 359
    target 551
    line_type "bus"
    line_number "108"
  ]
  edge [
    source 359
    target 615
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 359
    target 623
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 360
    target 438
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 361
    target 880
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 362
    target 569
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 363
    target 593
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 363
    target 825
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 364
    target 554
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 364
    target 579
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 365
    target 923
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 365
    target 704
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 366
    target 790
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 366
    target 784
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 367
    target 818
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 367
    target 599
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 368
    target 930
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 368
    target 483
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 370
    target 390
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 370
    target 603
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 371
    target 567
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 371
    target 779
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 373
    target 381
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 373
    target 937
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 373
    target 843
    line_type "sev"
    line_number "8"
  ]
  edge [
    source 374
    target 525
    line_type "bus"
    line_number "n7"
  ]
  edge [
    source 374
    target 548
    line_type "sev"
    line_number "8"
  ]
  edge [
    source 374
    target 835
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 375
    target 864
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 376
    target 658
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 377
    target 646
    line_type "str"
    line_number "11"
  ]
  edge [
    source 377
    target 800
    line_type "str"
    line_number "11"
  ]
  edge [
    source 378
    target 655
    line_type "bus"
    line_number "76"
  ]
  edge [
    source 378
    target 514
    line_type "bus"
    line_number "76"
  ]
  edge [
    source 379
    target 905
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 379
    target 628
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 380
    target 651
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 380
    target 815
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 381
    target 711
    line_type "str"
    line_number "10"
  ]
  edge [
    source 381
    target 937
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 382
    target 612
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 383
    target 956
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 383
    target 913
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 386
    target 504
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 386
    target 610
    line_type "str"
    line_number "11"
  ]
  edge [
    source 386
    target 719
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 387
    target 740
    line_type "sev"
    line_number "10"
  ]
  edge [
    source 387
    target 452
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 388
    target 435
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 388
    target 750
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 388
    target 464
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 389
    target 468
    line_type "bus"
    line_number "85"
  ]
  edge [
    source 389
    target 902
    line_type "str"
    line_number "9"
  ]
  edge [
    source 389
    target 741
    line_type "str"
    line_number "39"
  ]
  edge [
    source 390
    target 413
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 391
    target 534
    line_type "bus"
    line_number "67"
  ]
  edge [
    source 391
    target 671
    line_type "bus"
    line_number "67"
  ]
  edge [
    source 393
    target 768
    line_type "bus"
    line_number "196"
  ]
  edge [
    source 394
    target 633
    line_type "bus"
    line_number "85"
  ]
  edge [
    source 394
    target 702
    line_type "bus"
    line_number "85"
  ]
  edge [
    source 395
    target 530
    line_type "bus"
    line_number "n2"
  ]
  edge [
    source 396
    target 873
    line_type "bus"
    line_number "85"
  ]
  edge [
    source 397
    target 449
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 398
    target 775
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 398
    target 934
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 398
    target 840
    line_type "str"
    line_number "7"
  ]
  edge [
    source 398
    target 937
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 401
    target 851
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 402
    target 666
    line_type "str"
    line_number "10"
  ]
  edge [
    source 402
    target 477
    line_type "str"
    line_number "10"
  ]
  edge [
    source 403
    target 407
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 404
    target 564
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 406
    target 914
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 407
    target 844
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 408
    target 915
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 409
    target 738
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 409
    target 789
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 410
    target 767
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 411
    target 799
    line_type "bus"
    line_number "n1"
  ]
  edge [
    source 412
    target 935
    line_type "bus"
    line_number "n7"
  ]
  edge [
    source 412
    target 697
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 413
    target 567
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 415
    target 499
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 417
    target 573
    line_type "bus"
    line_number "61"
  ]
  edge [
    source 417
    target 570
    line_type "bus"
    line_number "61"
  ]
  edge [
    source 418
    target 874
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 418
    target 578
    line_type "bus"
    line_number "162"
  ]
  edge [
    source 419
    target 750
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 421
    target 753
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 422
    target 597
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 423
    target 853
    line_type "bus"
    line_number "76"
  ]
  edge [
    source 423
    target 926
    line_type "bus"
    line_number "76"
  ]
  edge [
    source 423
    target 516
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 423
    target 469
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 424
    target 821
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 425
    target 770
    line_type "str"
    line_number "8"
  ]
  edge [
    source 427
    target 544
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 428
    target 693
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 428
    target 536
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 429
    target 621
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 430
    target 501
    line_type "bus"
    line_number "175"
  ]
  edge [
    source 431
    target 868
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 431
    target 873
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 434
    target 488
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 436
    target 503
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 437
    target 889
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 437
    target 452
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 438
    target 832
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 439
    target 869
    line_type "str"
    line_number "7"
  ]
  edge [
    source 439
    target 534
    line_type "str"
    line_number "7"
  ]
  edge [
    source 441
    target 803
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 441
    target 941
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 441
    target 717
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 441
    target 581
    line_type "bus"
    line_number "176"
  ]
  edge [
    source 441
    target 510
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 443
    target 924
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 443
    target 560
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 444
    target 504
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 444
    target 882
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 445
    target 903
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 446
    target 660
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 447
    target 680
    line_type "str"
    line_number "34"
  ]
  edge [
    source 447
    target 944
    line_type "str"
    line_number "34"
  ]
  edge [
    source 448
    target 505
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 450
    target 622
    line_type "sev"
    line_number "10"
  ]
  edge [
    source 450
    target 654
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 450
    target 539
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 451
    target 620
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 451
    target 672
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 453
    target 861
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 454
    target 494
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 455
    target 936
    line_type "bus"
    line_number "60"
  ]
  edge [
    source 455
    target 686
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 455
    target 953
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 455
    target 919
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 456
    target 908
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 457
    target 879
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 458
    target 718
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 459
    target 599
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 460
    target 472
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 460
    target 939
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 461
    target 756
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 462
    target 713
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 462
    target 590
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 463
    target 472
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 465
    target 626
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 465
    target 708
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 467
    target 664
    line_type "bus"
    line_number "90"
  ]
  edge [
    source 467
    target 608
    line_type "str"
    line_number "9"
  ]
  edge [
    source 468
    target 802
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 468
    target 734
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 469
    target 926
    line_type "bus"
    line_number "76"
  ]
  edge [
    source 469
    target 827
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 469
    target 642
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 470
    target 821
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 471
    target 838
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 472
    target 591
    line_type "str"
    line_number "39"
  ]
  edge [
    source 472
    target 607
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 473
    target 955
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 474
    target 847
    line_type "bus"
    line_number "161"
  ]
  edge [
    source 474
    target 557
    line_type "bus"
    line_number "161"
  ]
  edge [
    source 475
    target 857
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 475
    target 593
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 476
    target 852
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 476
    target 618
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 479
    target 556
    line_type "bus"
    line_number "61"
  ]
  edge [
    source 479
    target 573
    line_type "bus"
    line_number "61"
  ]
  edge [
    source 481
    target 719
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 482
    target 668
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 482
    target 675
    line_type "bus"
    line_number "n7"
  ]
  edge [
    source 483
    target 796
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 484
    target 506
    line_type "bus"
    line_number "60"
  ]
  edge [
    source 484
    target 782
    line_type "bus"
    line_number "n2"
  ]
  edge [
    source 484
    target 952
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 485
    target 657
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 486
    target 503
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 486
    target 950
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 487
    target 770
    line_type "bus"
    line_number "161"
  ]
  edge [
    source 488
    target 640
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 488
    target 529
    line_type "bus"
    line_number "n3"
  ]
  edge [
    source 489
    target 619
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 489
    target 640
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 489
    target 842
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 490
    target 733
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 490
    target 809
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 491
    target 598
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 491
    target 499
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 492
    target 845
    line_type "bus"
    line_number "n1"
  ]
  edge [
    source 492
    target 506
    line_type "bus"
    line_number "n1"
  ]
  edge [
    source 492
    target 679
    line_type "bus"
    line_number "n3"
  ]
  edge [
    source 493
    target 897
    line_type "bus"
    line_number "n3"
  ]
  edge [
    source 493
    target 869
    line_type "str"
    line_number "7"
  ]
  edge [
    source 493
    target 856
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 495
    target 696
    line_type "bus"
    line_number "n1"
  ]
  edge [
    source 496
    target 672
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 497
    target 508
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 497
    target 727
    line_type "bus"
    line_number "176"
  ]
  edge [
    source 498
    target 552
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 498
    target 627
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 500
    target 922
    line_type "bus"
    line_number "86"
  ]
  edge [
    source 502
    target 867
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 502
    target 807
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 503
    target 858
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 504
    target 923
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 504
    target 870
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 505
    target 621
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 506
    target 782
    line_type "str"
    line_number "32"
  ]
  edge [
    source 507
    target 820
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 510
    target 650
    line_type "bus"
    line_number "175"
  ]
  edge [
    source 511
    target 607
    line_type "sev"
    line_number "10"
  ]
  edge [
    source 511
    target 654
    line_type "sev"
    line_number "10"
  ]
  edge [
    source 512
    target 624
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 513
    target 819
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 516
    target 606
    line_type "bus"
    line_number "76"
  ]
  edge [
    source 516
    target 853
    line_type "bus"
    line_number "76"
  ]
  edge [
    source 516
    target 831
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 517
    target 744
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 518
    target 871
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 519
    target 553
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 521
    target 535
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 521
    target 626
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 522
    target 850
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 522
    target 826
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 523
    target 769
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 523
    target 755
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 524
    target 836
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 525
    target 549
    line_type "bus"
    line_number "n7"
  ]
  edge [
    source 526
    target 643
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 527
    target 848
    line_type "bus"
    line_number "90"
  ]
  edge [
    source 527
    target 731
    line_type "bus"
    line_number "90"
  ]
  edge [
    source 530
    target 543
    line_type "bus"
    line_number "n17"
  ]
  edge [
    source 533
    target 543
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 535
    target 806
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 535
    target 875
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 536
    target 669
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 537
    target 788
    line_type "str"
    line_number "11"
  ]
  edge [
    source 538
    target 952
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 539
    target 574
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 540
    target 931
    line_type "bus"
    line_number "161"
  ]
  edge [
    source 542
    target 689
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 543
    target 889
    line_type "bus"
    line_number "89"
  ]
  edge [
    source 544
    target 684
    line_type "bus"
    line_number "63"
  ]
  edge [
    source 544
    target 584
    line_type "bus"
    line_number "63"
  ]
  edge [
    source 545
    target 871
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 546
    target 549
    line_type "str"
    line_number "7"
  ]
  edge [
    source 547
    target 917
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 548
    target 892
    line_type "sev"
    line_number "8"
  ]
  edge [
    source 548
    target 918
    line_type "str"
    line_number "7"
  ]
  edge [
    source 549
    target 700
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 549
    target 843
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 550
    target 894
    line_type "bus"
    line_number "83"
  ]
  edge [
    source 550
    target 699
    line_type "bus"
    line_number "83"
  ]
  edge [
    source 554
    target 602
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 555
    target 953
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 557
    target 951
    line_type "bus"
    line_number "161"
  ]
  edge [
    source 558
    target 824
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 559
    target 636
    line_type "str"
    line_number "10"
  ]
  edge [
    source 560
    target 818
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 562
    target 931
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 562
    target 940
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 563
    target 567
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 565
    target 898
    line_type "sev"
    line_number "10"
  ]
  edge [
    source 565
    target 601
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 566
    target 715
    line_type "bus"
    line_number "85"
  ]
  edge [
    source 567
    target 738
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 568
    target 816
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 569
    target 760
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 570
    target 653
    line_type "bus"
    line_number "61"
  ]
  edge [
    source 571
    target 896
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 571
    target 865
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 572
    target 877
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 575
    target 725
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 575
    target 787
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 576
    target 922
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 577
    target 704
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 577
    target 789
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 579
    target 942
    line_type "bus"
    line_number "n17"
  ]
  edge [
    source 579
    target 793
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 580
    target 954
    line_type "bus"
    line_number "e"
  ]
  edge [
    source 581
    target 941
    line_type "bus"
    line_number "143"
  ]
  edge [
    source 582
    target 891
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 582
    target 628
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 583
    target 685
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 584
    target 696
    line_type "bus"
    line_number "63"
  ]
  edge [
    source 586
    target 595
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 586
    target 949
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 587
    target 617
    line_type "str"
    line_number "7"
  ]
  edge [
    source 587
    target 901
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 587
    target 846
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 588
    target 817
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 589
    target 773
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 589
    target 928
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 592
    target 739
    line_type "bus"
    line_number "161"
  ]
  edge [
    source 592
    target 634
    line_type "bus"
    line_number "162"
  ]
  edge [
    source 594
    target 783
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 596
    target 834
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 596
    target 651
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 597
    target 643
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 598
    target 929
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 600
    target 705
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 601
    target 909
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 604
    target 748
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 605
    target 772
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 606
    target 655
    line_type "bus"
    line_number "76"
  ]
  edge [
    source 608
    target 674
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 609
    target 731
    line_type "bus"
    line_number "90"
  ]
  edge [
    source 609
    target 751
    line_type "bus"
    line_number "90"
  ]
  edge [
    source 610
    target 788
    line_type "str"
    line_number "11"
  ]
  edge [
    source 611
    target 850
    line_type "str"
    line_number "9"
  ]
  edge [
    source 613
    target 800
    line_type "sev"
    line_number "11"
  ]
  edge [
    source 615
    target 673
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 616
    target 701
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 618
    target 683
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 619
    target 940
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 620
    target 912
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 624
    target 820
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 625
    target 892
    line_type "sev"
    line_number "8"
  ]
  edge [
    source 625
    target 823
    line_type "bus"
    line_number "n7"
  ]
  edge [
    source 625
    target 765
    line_type "str"
    line_number "10"
  ]
  edge [
    source 627
    target 735
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 629
    target 739
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 630
    target 696
    line_type "bus"
    line_number "n1"
  ]
  edge [
    source 632
    target 913
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 632
    target 684
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 633
    target 932
    line_type "bus"
    line_number "85"
  ]
  edge [
    source 634
    target 681
    line_type "bus"
    line_number "162"
  ]
  edge [
    source 635
    target 810
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 635
    target 939
    line_type "bus"
    line_number "n9"
  ]
  edge [
    source 635
    target 884
    line_type "bus"
    line_number "n9"
  ]
  edge [
    source 636
    target 774
    line_type "str"
    line_number "9"
  ]
  edge [
    source 637
    target 955
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 638
    target 675
    line_type "bus"
    line_number "n7"
  ]
  edge [
    source 639
    target 838
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 641
    target 901
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 644
    target 909
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 645
    target 891
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 646
    target 800
    line_type "sev"
    line_number "11"
  ]
  edge [
    source 647
    target 779
    line_type "bus"
    line_number "88"
  ]
  edge [
    source 648
    target 797
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 648
    target 806
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 648
    target 837
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 649
    target 894
    line_type "bus"
    line_number "83"
  ]
  edge [
    source 650
    target 822
    line_type "bus"
    line_number "172"
  ]
  edge [
    source 656
    target 914
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 656
    target 700
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 661
    target 948
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 662
    target 766
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 664
    target 907
    line_type "bus"
    line_number "90"
  ]
  edge [
    source 665
    target 948
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 666
    target 765
    line_type "str"
    line_number "10"
  ]
  edge [
    source 667
    target 844
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 671
    target 927
    line_type "bus"
    line_number "67"
  ]
  edge [
    source 674
    target 907
    line_type "bus"
    line_number "90"
  ]
  edge [
    source 676
    target 705
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 678
    target 951
    line_type "bus"
    line_number "161"
  ]
  edge [
    source 679
    target 793
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 679
    target 906
    line_type "bus"
    line_number "74"
  ]
  edge [
    source 680
    target 953
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 680
    target 823
    line_type "bus"
    line_number "n7"
  ]
  edge [
    source 681
    target 723
    line_type "bus"
    line_number "162"
  ]
  edge [
    source 682
    target 688
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 683
    target 887
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 686
    target 777
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 687
    target 845
    line_type "bus"
    line_number "n1"
  ]
  edge [
    source 687
    target 906
    line_type "str"
    line_number "3"
  ]
  edge [
    source 692
    target 787
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 694
    target 956
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 695
    target 753
    line_type "bus"
    line_number "190"
  ]
  edge [
    source 697
    target 698
    line_type "bus"
    line_number "73"
  ]
  edge [
    source 702
    target 807
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 703
    target 876
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 703
    target 805
    line_type "bus"
    line_number "108"
  ]
  edge [
    source 708
    target 774
    line_type "str"
    line_number "9"
  ]
  edge [
    source 709
    target 855
    line_type "str"
    line_number "12"
  ]
  edge [
    source 709
    target 775
    line_type "str"
    line_number "12"
  ]
  edge [
    source 711
    target 740
    line_type "str"
    line_number "10"
  ]
  edge [
    source 713
    target 726
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 716
    target 827
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 716
    target 919
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 716
    target 777
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 718
    target 829
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 720
    target 845
    line_type "bus"
    line_number "60"
  ]
  edge [
    source 721
    target 911
    line_type "bus"
    line_number "n1"
  ]
  edge [
    source 722
    target 888
    line_type "bus"
    line_number "131"
  ]
  edge [
    source 725
    target 865
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 727
    target 814
    line_type "bus"
    line_number "n6"
  ]
  edge [
    source 727
    target 746
    line_type "bus"
    line_number "197"
  ]
  edge [
    source 727
    target 943
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 727
    target 878
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 728
    target 740
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 729
    target 825
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 729
    target 808
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 730
    target 739
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 735
    target 920
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 737
    target 836
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 740
    target 937
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 740
    target 944
    line_type "str"
    line_number "34"
  ]
  edge [
    source 741
    target 902
    line_type "bus"
    line_number "n4"
  ]
  edge [
    source 742
    target 841
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 743
    target 809
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 744
    target 747
    line_type "bus"
    line_number "756"
  ]
  edge [
    source 746
    target 759
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 746
    target 801
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 748
    target 924
    line_type "bus"
    line_number "650"
  ]
  edge [
    source 751
    target 797
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 755
    target 872
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 759
    target 943
    line_type "bus"
    line_number "176"
  ]
  edge [
    source 759
    target 814
    line_type "bus"
    line_number "173"
  ]
  edge [
    source 761
    target 902
    line_type "str"
    line_number "12"
  ]
  edge [
    source 762
    target 872
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 763
    target 824
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 764
    target 895
    line_type "bus"
    line_number "bus22220"
  ]
  edge [
    source 764
    target 873
    line_type "bus"
    line_number "80"
  ]
  edge [
    source 767
    target 816
    line_type "bus"
    line_number "691"
  ]
  edge [
    source 769
    target 866
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 770
    target 771
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 770
    target 842
    line_type "bus"
    line_number "65"
  ]
  edge [
    source 771
    target 842
    line_type "bus"
    line_number "66"
  ]
  edge [
    source 772
    target 904
    line_type "bus"
    line_number "120"
  ]
  edge [
    source 773
    target 905
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 775
    target 840
    line_type "str"
    line_number "32"
  ]
  edge [
    source 776
    target 910
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 776
    target 831
    line_type "bus"
    line_number "141"
  ]
  edge [
    source 777
    target 944
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 780
    target 875
    line_type "bus"
    line_number "77"
  ]
  edge [
    source 780
    target 881
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 782
    target 811
    line_type "bus"
    line_number "n2"
  ]
  edge [
    source 785
    target 935
    line_type "bus"
    line_number "72"
  ]
  edge [
    source 785
    target 854
    line_type "bus"
    line_number "72"
  ]
  edge [
    source 793
    target 942
    line_type "bus"
    line_number "n3"
  ]
  edge [
    source 796
    target 916
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 802
    target 932
    line_type "bus"
    line_number "85"
  ]
  edge [
    source 804
    target 909
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 805
    target 809
    line_type "bus"
    line_number "70"
  ]
  edge [
    source 805
    target 876
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 807
    target 933
    line_type "bus"
    line_number "n5"
  ]
  edge [
    source 812
    target 938
    line_type "str"
    line_number "7"
  ]
  edge [
    source 815
    target 858
    line_type "bus"
    line_number "62"
  ]
  edge [
    source 823
    target 918
    line_type "str"
    line_number "7"
  ]
  edge [
    source 827
    target 896
    line_type "bus"
    line_number "n8"
  ]
  edge [
    source 829
    target 885
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 830
    target 860
    line_type "bus"
    line_number "412"
  ]
  edge [
    source 831
    target 853
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 833
    target 866
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 833
    target 846
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 841
    target 928
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 843
    target 944
    line_type "bus"
    line_number "690"
  ]
  edge [
    source 851
    target 877
    line_type "bus"
    line_number "192"
  ]
  edge [
    source 855
    target 902
    line_type "str"
    line_number "12"
  ]
  edge [
    source 856
    target 938
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 856
    target 897
    line_type "str"
    line_number "15"
  ]
  edge [
    source 876
    target 890
    line_type "bus"
    line_number "107"
  ]
  edge [
    source 876
    target 947
    line_type "bus"
    line_number "106"
  ]
  edge [
    source 879
    target 880
    line_type "bus"
    line_number "75"
  ]
  edge [
    source 884
    target 909
    line_type "bus"
    line_number "n9"
  ]
  edge [
    source 885
    target 922
    line_type "bus"
    line_number "81"
  ]
  edge [
    source 896
    target 919
    line_type "str"
    line_number "34"
  ]
  edge [
    source 900
    target 933
    line_type "sev"
    line_number "16"
  ]
  edge [
    source 909
    target 910
    line_type "bus"
    line_number "79"
  ]
  edge [
    source 917
    target 945
    line_type "bus"
    line_number "130"
  ]
  edge [
    source 942
    target 946
    line_type "bus"
    line_number "n17"
  ]
]
